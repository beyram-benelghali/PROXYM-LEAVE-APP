package com.proxym.proxymleave.login;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.proxym.proxymleave.common.Utils;
import com.proxym.proxymleave.login.event.LoginEvent;
import com.proxym.proxymleave.login.event.ResponseLoginEvent;
import com.proxym.proxymleave.models.manager.UserManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.ref.WeakReference;

/**
 * Created by beyram on 20/06/17.
 */

public class LoginPresenter implements ILogin_MVP.PresenterModelOps, ILogin_MVP.PresenterViewOps {

    // View reference
    private WeakReference<ILogin_MVP.ViewOps> mView;
    // Model reference
    private ILogin_MVP.ModelOps mModel;
    private Context context;
    private EventBus bus = EventBus.getDefault();

    /**
     * Presenter Constructor
     */
    public LoginPresenter(ILogin_MVP.ViewOps view) {
        mView = new WeakReference<>(view);
        context = mView.get().getActivityContext();
        bus.register(this);
    }

    /**
     * get View from LoginActivity
     *
     * @return
     * @throws NullPointerException
     */
    private ILogin_MVP.ViewOps getView() throws NullPointerException {
        if (mView != null) {
            return mView.get();
        } else {
            throw new NullPointerException("View is unavailable");
        }
    }

    /**
     * set View for LoginPresenter
     *
     * @param view
     */
    public void setView(ILogin_MVP.ViewOps view) {
        mView = new WeakReference<>(view);
    }

    /**
     * Unregister eventBus after loginSuccess
     */
    @Override
    public void unregisterEventBus() {
        bus.unregister(this);
    }

    /**
     * set model for Presenter
     *
     * @param model
     */
    public void setModel(ILogin_MVP.ModelOps model) {
        mModel = model;
    }

    /**
     * Methode for hanling LoginEvent from View- eventBus
     *
     * @param l
     */
    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void loginUser(LoginEvent l) {
        mModel.login(l.getUsername(), l.getPassword());
    }

    /**
     * Methode for hanling ResponseLoginEvent to check if User is authenticated or not
     *
     * @param l
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void loginState(ResponseLoginEvent l) {
        getView().hideProgress();
        if (l.getMessage() == null) {
            if (!l.isValid()) {
                Toast.makeText(context, "User Not Exist", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, "Welcome " + UserManager.getCurrentUser().getDisplayName(), Toast.LENGTH_SHORT).show();
                getView().navToMainActivity();
            }
        } else {
            Utils.showAlert((Activity) context, "Erreur", l.getMessage());
        }

    }
}

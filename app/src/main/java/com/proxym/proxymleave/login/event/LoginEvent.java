package com.proxym.proxymleave.login.event;

/**
 * Created by beyram on 05/07/17.
 */

/**
 * EventBus Event to authenticate user
 */
public class LoginEvent {

    private String username;
    private String password;

    public LoginEvent(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}

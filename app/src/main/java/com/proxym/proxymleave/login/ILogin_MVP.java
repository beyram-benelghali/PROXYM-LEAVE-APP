package com.proxym.proxymleave.login;

import android.content.Context;

/**
 * Created by beyram on 20/06/17.
 */

public interface ILogin_MVP {

    /**
     * View methods available to Presenter.
     */
    interface ViewOps {
        Context getActivityContext();

        void hideProgress();

        void navToMainActivity();
    }

    /**
     * Presenter Operations offered to View
     */
    interface PresenterViewOps {
        void setView(ViewOps view);

        void unregisterEventBus();
    }

    /**
     * Presenter Operations offered to Model
     */
    interface PresenterModelOps {
    }

    /**
     * Models Operations offered to Presenter
     */
    interface ModelOps {
        void login(String login, String password);
    }

}

package com.proxym.proxymleave.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.proxym.proxymleave.R;
import com.proxym.proxymleave.base.BaseActivity;
import com.proxym.proxymleave.common.CheckInternetTask;
import com.proxym.proxymleave.common.ICheckInternet;
import com.proxym.proxymleave.common.Utils;
import com.proxym.proxymleave.login.event.LoginEvent;
import com.proxym.proxymleave.main.MainActivity;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;

public class LoginActivity extends BaseActivity implements View.OnClickListener, ILogin_MVP.ViewOps, ICheckInternet {

    @BindView(R.id.loginButton)
    Button loginButton;
    @BindView(R.id.usernameLogin)
    EditText editTextUsername;
    @BindView(R.id.passwordLogin)
    EditText editTextPassword;
    String username;
    String password;
    private ILogin_MVP.PresenterViewOps mPresenter;
    private ProgressDialog progress;
    private EventBus bus = EventBus.getDefault();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginButton.setOnClickListener(this);
        progress = new ProgressDialog(this);
        editTextPassword.setTransformationMethod(new PasswordTransformationMethod());
    }

    /**
     * SETUP MVP Design Pattern
     */
    @Override
    protected void initializeMVP() {
        LoginPresenter presenter = new LoginPresenter(this);
        LoginModel model = new LoginModel(presenter);
        presenter.setModel(model);
        mPresenter = presenter;
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_login;
    }

    /**
     * Handling login button click
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginButton: {
                username = editTextUsername.getText().toString();
                password = editTextPassword.getText().toString();
                if (username.isEmpty() || password.isEmpty()) {
                    Utils.showAlert(this, "Erreur", "Veuillez saisir vos identifiants !");
                } else if (!Utils.isValidEmail(username)) {
                    Utils.showAlert(this, "Erreur", "Verifier votre E-MAIL");
                } else {
                    CheckInternetTask checkTask = new CheckInternetTask(this, this, progress);
                    checkTask.execute();
                }
            }
        }
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    /**
     * dismiss ProgressDialog after loading holidays
     */
    @Override
    public void hideProgress() {
        progress.dismiss();
    }

    /**
     * Navigate to MainActivity after loginSuccess
     */
    @Override
    public void navToMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.unregisterEventBus();
    }

    /**
     * Post LoginEvent after checking internet connection
     */
    @Override
    public void successCheckConnexion() {
        bus.post(new LoginEvent(username, password));
    }
}

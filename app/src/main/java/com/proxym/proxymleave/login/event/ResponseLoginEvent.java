package com.proxym.proxymleave.login.event;

/**
 * Created by beyram on 04/07/17.
 */

/**
 * EventBus Event to check  if user exist or not
 */
public class ResponseLoginEvent {
    boolean valid;
    String message;

    public ResponseLoginEvent(boolean valid) {
        this.valid = valid;
    }

    public ResponseLoginEvent(boolean valid, String message) {
        this.valid = valid;
        this.message = message;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

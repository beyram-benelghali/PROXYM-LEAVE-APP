package com.proxym.proxymleave.login;

import com.proxym.proxymleave.login.event.ResponseLoginEvent;
import com.proxym.proxymleave.models.api.IProxymFake;
import com.proxym.proxymleave.models.api.RestClientFake;
import com.proxym.proxymleave.models.api.response.LoginResponse;
import com.proxym.proxymleave.models.manager.UserManager;

import org.greenrobot.eventbus.EventBus;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by beyram on 29/06/17.
 */

public class LoginModel implements ILogin_MVP.ModelOps {

    ILogin_MVP.PresenterModelOps mPresenter;
    IProxymFake service;
    private EventBus bus = EventBus.getDefault();

    public LoginModel(ILogin_MVP.PresenterModelOps presenter) {
        this.mPresenter = presenter;
        service = RestClientFake.getInstance().getProxymLeaveAPI();
    }

    /**
     * Authenticate to Server and Returning JSESSIONID
     *
     * @param login
     * @param password
     */
    @Override
    public void login(final String login, final String password) {
        Call<LoginResponse> call = service.login();
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
//                Log.v("JSESSIONID",UserManager.getJessionId());
                if (response.body().getSuccess() == true) {
                    UserManager.setPassword(password);
                    UserManager.setUsername(login);
                    UserManager.setCurrentUser(response.body().getEmployee());
                }
                bus.post(new ResponseLoginEvent(response.body().getSuccess()));
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                bus.post(new ResponseLoginEvent(false, "Server is down !"));
            }
        });
    }

}

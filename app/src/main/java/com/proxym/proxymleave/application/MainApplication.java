package com.proxym.proxymleave.application;

import android.app.Application;
import android.util.Log;

import com.google.firebase.crash.FirebaseCrash;

/**
 * Created by beyram on 7/25/17.
 */

public class MainApplication extends Application {

    private Thread.UncaughtExceptionHandler UExceptionHandler;

    public MainApplication() {
        super();
        initializeUExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(UExceptionHandler);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    void initializeUExceptionHandler() {
        UExceptionHandler = new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                Log.v("uncaughtException", e.toString());
                e.printStackTrace();
                FirebaseCrash.report(e);
                //System.exit(0);
            }
        };
    }

}

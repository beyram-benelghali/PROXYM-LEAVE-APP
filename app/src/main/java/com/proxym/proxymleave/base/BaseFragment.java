package com.proxym.proxymleave.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.proxym.proxymleave.R;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by beyram on 20/06/17.
 */

public abstract class BaseFragment extends Fragment implements IFragmentOnBackClick {

    ActionBar actionBar;
    Unbinder unbinder;
    private View view;

    protected abstract void initializePresenter();

    public abstract int getLayoutId();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializePresenter();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(getLayoutId(), container, false);
        return view;
    }

    /**
     * Bind View onViewCreated
     *
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
    }

    /**
     * UnBind View onDestroyView
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    /**
     * set Title for ActionBar
     *
     * @param title
     */
    public void setTitle(String title) {
        actionBar = ((BaseActivity) getActivity()).getSupportActionBar();
        if (actionBar != null) {
            TextView titleTextView = ButterKnife.findById(getActivity(), R.id.txt_toolbar_title);
            titleTextView.setText(title);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

}

package com.proxym.proxymleave.base;

/**
 * Created by beyram on 7/13/17.
 */

/**
 * Handle Back Button Event for Fragment
 */
public interface IFragmentOnBackClick {
    void onBackPressed();
}

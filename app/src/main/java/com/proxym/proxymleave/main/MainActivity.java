package com.proxym.proxymleave.main;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.proxym.proxymleave.R;
import com.proxym.proxymleave.base.BaseActivity;
import com.proxym.proxymleave.common.IInternetBReciever;
import com.proxym.proxymleave.common.InternetBReciever;
import com.proxym.proxymleave.login.LoginActivity;
import com.proxym.proxymleave.models.manager.UserManager;
import com.proxym.proxymleave.trackTimeAdmin.autorisationList.ListAutorisationFragment;
import com.proxym.proxymleave.trackTimeAdmin.dashboardAdmin.DashboardAdminFragment;
import com.proxym.proxymleave.trackTimeAdmin.leaveList.ListCongesFragment;
import com.proxym.proxymleave.trackTimeAdmin.pointageList.PointageListFragment;
import com.proxym.proxymleave.trackTimeAdmin.updateDeplacement.DeplacementUpdateFragment;
import com.proxym.proxymleave.trackTimeEmployee.dashboard.DashboardFragment;
import com.proxym.proxymleave.trackTimeEmployee.holiday.HolidayFragment;
import com.proxym.proxymleave.trackTimeEmployee.requestLeave.RequestLeaveFragment;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, IInternetBReciever {

    DrawerLayout drawer;
    @BindView(R.id.coordinatorLayoutMain)
    CoordinatorLayout coordinatorLayoutMain;
    Snackbar snackNetwork;
    InternetBReciever internetBReciever;
    private FirebaseAnalytics mFirebaseAnalytics;

    /**
     * register Internet Broadcast Reciever to check internet connection
     * show dashboard fragment
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TrackTime_Login_Success Firebase Analytics Event
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        mFirebaseAnalytics.setUserId(UserManager.getUsername());
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, UserManager.getCurrentUser().getEmail());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, UserManager.getCurrentUser().getDisplayName());
        mFirebaseAnalytics.logEvent("TrackTime_Login_Success", bundle);

        setUpActionBar();
        DashboardFragment fragment = new DashboardFragment();
        navToFragment(fragment, "DashboardFragment");
        IntentFilter filter1 = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        internetBReciever = new InternetBReciever(this);
        registerReceiver(internetBReciever, filter1);
    }

    @Override
    protected void initializeMVP() {
    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    /**
     * SETUP NavigationDrawer and ActionBar
     */
    public void setUpActionBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        toggle.setDrawerIndicatorEnabled(false);
        Drawable drawable = ResourcesCompat.getDrawable(getResources(), R.drawable.icn_menu, getTheme());
        toggle.setHomeAsUpIndicator(drawable);
        toggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (drawer.isDrawerVisible(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                } else {
                    drawer.openDrawer(GravityCompat.START);
                }
            }
        });
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View headerLayout = navigationView.getHeaderView(0);
        TextView headerName = (TextView) headerLayout.findViewById(R.id.nameEmployeeNavHeader);
        CircleImageView headerImg = (CircleImageView) headerLayout.findViewById(R.id.userPic);
        Glide.with(this)
                .load("https://img4.hostingpics.net/pics/5303753b7d6f60e2d450b899c322266fc6edfd.jpg")
                .into(headerImg);
        headerName.setText(UserManager.getCurrentUser().getDisplayName());
        navigationView.setNavigationItemSelectedListener(this);
    }

    /**
     * Handle Navigation Drawer menu click
     *
     * @param item
     * @return
     */
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        drawer.closeDrawer(GravityCompat.START);
        switch (item.getItemId()) {
            case R.id.dashboardMenu: {
                DashboardFragment dashboardFragment = new DashboardFragment();
                navToFragment(dashboardFragment, "DashboardFragment");
                break;
            }
            case R.id.addLeaveMenu: {
                RequestLeaveFragment requestLeaveFragment = new RequestLeaveFragment();
                navToFragment(requestLeaveFragment, "RequestLeaveFragment");
                break;
            }
            case R.id.holidayMenu: {
                HolidayFragment holidayFragment = new HolidayFragment();
                navToFragment(holidayFragment, "HolidayFragment");
                break;
            }
            case R.id.updateDepla: {
                DeplacementUpdateFragment fragment = new DeplacementUpdateFragment();
                navToFragment(fragment, "DeplacementUpdateFragment");
                break;
            }
            case R.id.dashboardAdminMenu: {
                DashboardAdminFragment fragment = new DashboardAdminFragment();
                navToFragment(fragment, "DashboardAdminFragment");
                break;
            }
            case R.id.logoutMenu: {
                Intent INT = new Intent(this, LoginActivity.class);
                startActivity(INT);
                finish();
                break;
            }
            case R.id.congeeListAdmin: {
                ListCongesFragment fragment = new ListCongesFragment();
                navToFragment(fragment, "ListCongesFragment");
                break;
            }
            case R.id.authListAdmin: {
                ListAutorisationFragment fragment = new ListAutorisationFragment();
                navToFragment(fragment, "ListAutorisationFragment");
                break;
            }
            case R.id.pointageListAdmin: {
                PointageListFragment fragment = new PointageListFragment();
                navToFragment(fragment, "PointageListFragment");
                break;
            }

        }
        return true;
    }

    /**
     * method for navigating to another fragment
     *
     * @param fragment
     * @param fragmentTAG
     */
    public void navToFragment(final android.support.v4.app.Fragment fragment, String fragmentTAG) {
        android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment, fragmentTAG);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    /**
     * handle back button event for fragments in activity
     */
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
        } else {
            if (onBackPressedListener != null) {
                onBackPressedListener.onBackPressed();
            } else {
                Log.v("BackDrawer", "BackDrawer");
                super.onBackPressed();
            }
        }
    }

    /**
     * Updating SnackBar if Internet connectivity is changed
     *
     * @param state
     */
    @Override
    public void updateSnackBar(int state) {
        switch (state) {
            case 1: {
                snackNetwork = Snackbar.make(coordinatorLayoutMain, "PAS DE CONNEXION INTERNET !", Snackbar.LENGTH_SHORT);
                snackNetwork.setDuration(Snackbar.LENGTH_INDEFINITE);
                snackNetwork.show();
                break;
            }
            case 2: {
                snackNetwork = Snackbar.make(coordinatorLayoutMain, "EN ATTENTE D'UNE CONNEXION INTERNET !", Snackbar.LENGTH_SHORT);
                snackNetwork.setDuration(Snackbar.LENGTH_INDEFINITE);
                snackNetwork.show();
                break;
            }
            default: {
                if (snackNetwork != null) {
                    snackNetwork.dismiss();
                }

            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(internetBReciever);
    }
}

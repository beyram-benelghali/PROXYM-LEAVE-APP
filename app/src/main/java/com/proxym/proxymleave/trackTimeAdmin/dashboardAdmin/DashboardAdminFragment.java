package com.proxym.proxymleave.trackTimeAdmin.dashboardAdmin;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.proxym.proxymleave.R;
import com.proxym.proxymleave.base.BaseFragment;
import com.proxym.proxymleave.models.manager.UserManager;
import com.proxym.proxymleave.trackTimeEmployee.requestLeave.RequestLeaveFragment;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardAdminFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.employeeName)
    TextView employeeName;
    @BindView(R.id.employeeDep)
    TextView employeeDep;
    @BindView(R.id.addCongeButton)
    FloatingActionButton addCongeButton;
    @BindView(R.id.dashboardImgUser)
    CircleImageView imgUser;

    public DashboardAdminFragment() {
        // Required empty public constructor
    }

    @Override
    protected void initializePresenter() {

    }

    @Override
    public int getLayoutId() {
        return 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_admin_dashboard, container, false);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getUserInformation();
        super.setTitle("DASHBOARD ADMIN");
        addCongeButton.setOnClickListener(this);
    }

    /**
     * Show User Information in Dashboard
     */
    private void getUserInformation() {
        employeeName.setText(UserManager.getCurrentUser().getDisplayName());
        employeeDep.setText(UserManager.getCurrentUser().getDepartment().get("name").getAsString());
        Glide.with(getActivity())
                .load("https://img4.hostingpics.net/pics/5303753b7d6f60e2d450b899c322266fc6edfd.jpg")
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        Glide.with(getActivity())
                                .load(R.drawable.errorloadimg)
                                .into(imgUser);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(imgUser);

    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addCongeButton: {
                android.support.v4.app.FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.container, new RequestLeaveFragment());
                transaction.addToBackStack(null);
                transaction.commit();
            }
        }
    }
}

package com.proxym.proxymleave.trackTimeAdmin.leaveList;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.proxym.proxymleave.R;
import com.proxym.proxymleave.models.entity.Transaction;
import com.proxym.proxymleave.trackTimeAdmin.requestLeaveUpdate.RequestLeaveUpdateFragment;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by beyram on 7/26/17.
 */

public class CongeRecycleViewAdapter extends RecyclerSwipeAdapter<CongeRecycleViewAdapter.ViewHolder> {

    List<Transaction> listTransaction;
    View rowView;
    private Context mContext;

    public CongeRecycleViewAdapter(Context mContext, List<Transaction> listTransaction) {
        this.listTransaction = listTransaction;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_congee_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(rowView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Transaction transaction = listTransaction.get(position);
        holder.dateDemande.setText(transaction.getBeginDate());
        holder.etatDemande.setText("En attente");
        holder.nbJourConge.setText("Nombre de jours : " + transaction.getTotalDays());
        holder.dateConge.setText(transaction.getBeginDate() + " - " + transaction.getEndDate());
        holder.typeCongee.setText(transaction.getCategoryName());
        holder.employeeName.setText(transaction.getEmployeeUsername());
        Glide.with(mContext)
                .load("https://img4.hostingpics.net/pics/5303753b7d6f60e2d450b899c322266fc6edfd.jpg")
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        Glide.with(mContext)
                                .load(R.drawable.errorloadimg)
                                .into(holder.imgEmployee);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(holder.imgEmployee);

        holder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {

            @Override
            public void onOpen(SwipeLayout layout) {
                //layout.
                mItemManger.closeAllExcept(layout);
            }

        });

        mItemManger.bindView(holder.itemView, position);

    }

    @Override
    public int getItemCount() {
        return (null != listTransaction ? listTransaction.size() : 0);
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipeCongee;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        LinearLayout linearLayoutRefuse, linearLayoutAccept, linearLayoutEdit;
        SwipeLayout swipeLayout;
        TextView dateDemande;
        TextView etatDemande;
        TextView nbJourConge;
        TextView dateConge;
        TextView typeCongee;
        TextView employeeName;
        CircleImageView imgEmployee;

        public ViewHolder(View view) {
            super(view);
            this.swipeLayout = (SwipeLayout) view.findViewById(R.id.swipeCongee);
            this.etatDemande = (TextView) view.findViewById(R.id.etatDemande);
            this.dateDemande = (TextView) view.findViewById(R.id.dateDemande);
            this.nbJourConge = (TextView) view.findViewById(R.id.nbJourConge);
            this.dateConge = (TextView) view.findViewById(R.id.dateConge);
            this.typeCongee = (TextView) view.findViewById(R.id.typeCongee);
            this.employeeName = (TextView) view.findViewById(R.id.employeeName);
            this.imgEmployee = (CircleImageView) view.findViewById(R.id.imgEmployee);
            this.linearLayoutRefuse = (LinearLayout) view.findViewById(R.id.linearLayoutRefuse);
            this.linearLayoutAccept = (LinearLayout) view.findViewById(R.id.linearLayoutAccept);
            this.linearLayoutEdit = (LinearLayout) view.findViewById(R.id.linearLayoutEdit);
            linearLayoutEdit.setOnClickListener(this);
            linearLayoutAccept.setOnClickListener(this);
            linearLayoutRefuse.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.linearLayoutRefuse: {
                    Toast.makeText(mContext, "Refuse", Toast.LENGTH_SHORT).show();
                    break;
                }
                case R.id.linearLayoutEdit: {
                    Toast.makeText(mContext, "RequestLeaveUpdate", Toast.LENGTH_SHORT).show();
                    RequestLeaveUpdateFragment fragment = new RequestLeaveUpdateFragment();
                    android.support.v4.app.FragmentTransaction transaction = ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container, fragment, "RequestLeaveUpdateFragment");
                    transaction.addToBackStack(null);
                    transaction.commit();
                    break;
                }
                case R.id.linearLayoutAccept: {
                    Toast.makeText(mContext, "Accept", Toast.LENGTH_SHORT).show();
                    break;
                }
            }
        }
    }

}

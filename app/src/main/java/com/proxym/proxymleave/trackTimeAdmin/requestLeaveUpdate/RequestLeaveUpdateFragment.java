package com.proxym.proxymleave.trackTimeAdmin.requestLeaveUpdate;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proxym.proxymleave.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class RequestLeaveUpdateFragment extends Fragment {

    public RequestLeaveUpdateFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_request_leave_update, container, false);
    }

}

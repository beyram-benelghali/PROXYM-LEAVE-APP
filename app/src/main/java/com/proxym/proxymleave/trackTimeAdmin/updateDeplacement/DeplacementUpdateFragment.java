package com.proxym.proxymleave.trackTimeAdmin.updateDeplacement;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.proxym.proxymleave.R;
import com.proxym.proxymleave.base.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class DeplacementUpdateFragment extends BaseFragment {

    public DeplacementUpdateFragment() {
        // Required empty public constructor
    }

    @Override
    protected void initializePresenter() {

    }

    @Override
    public int getLayoutId() {
        return 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.setTitle("MAJ DEPLACEMENT");
        return inflater.inflate(R.layout.fragment_deplacement_update, container, false);
    }

    @Override
    public void onBackPressed() {

    }
}

package com.proxym.proxymleave.trackTimeAdmin.pointageList;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.proxym.proxymleave.R;
import com.proxym.proxymleave.base.BaseActivity;
import com.proxym.proxymleave.base.BaseFragment;
import com.proxym.proxymleave.common.Utils;
import com.proxym.proxymleave.models.entity.Pointage;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class PointageListFragment extends BaseFragment implements View.OnClickListener {

    List<Pointage> pointageList;
    @BindView(R.id.listPointage)
    RecyclerView listPointage;
    LinearLayoutManager mLayoutManager;
    @BindView(R.id.RequestImgUser)
    CircleImageView imgUser;
    @BindView(R.id.salSpinner)
    Spinner salSpinner;
    @BindView(R.id.departSpinner)
    Spinner departSpinner;
    @BindView(R.id.exportAuth)
    Button exportAuth;
    @BindView(R.id.filterAuth)
    Button filterAuth;
    private ProgressDialog progress;
    public PointageListFragment() {
        // Required empty public constructor
    }

    @Override
    protected void initializePresenter() {

    }

    @Override
    public int getLayoutId() {
        return 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_pointage_list, container, false);
        super.setTitle("POINTAGES");
        progress = new ProgressDialog(getActivity());
        ((BaseActivity) getActivity()).setOnBackPressedListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listPointage.setHasFixedSize(true);
        filterAuth.setOnClickListener(this);
        exportAuth.setOnClickListener(this);
        pointageList = new ArrayList<>();
        Pointage p = new Pointage();
        Pointage p2 = new Pointage();
        Pointage p3 = new Pointage();

        ArrayList<Date> timePointages = new ArrayList<>();
        ArrayList<Date> timePointages2 = new ArrayList<>();
        ArrayList<Date> timePointages3 = new ArrayList<>();

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date date1 = formatter.parse("2017-07-28 09:50:01");
            Date date11 = formatter.parse("2017-07-28 08:30:01");
            Date date2 = formatter.parse("2017-07-28 12:30:01");
            Date date3 = formatter.parse("2017-07-28 14:00:01");

            Date date4 = formatter.parse("2017-07-28 18:00:01");

            Date date0 = formatter.parse("2017-07-28 19:00:01");

            timePointages.add(date11);
            timePointages2.add(date1);
            timePointages3.add(date1);
            timePointages.add(date2);
            timePointages2.add(date2);
            timePointages3.add(date2);
            timePointages.add(date3);
            timePointages2.add(date3);
            timePointages3.add(date4);
            timePointages.add(date4);
            timePointages2.add(date0);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        p.setPointageTimes(timePointages);
        p.setEmployeeMatricule("1789");
        p.setEmployeeUsername("Ahmed Mohamed");
        p.setPourcentage(20);
        p.setTime("08:01:32");

        p2.setEmployeeMatricule("1789");
        p2.setEmployeeUsername("Beyram Ghali");
        p2.setPourcentage(20);
        p2.setTime("09:01:32");
        p2.setPointageTimes(timePointages2);

        p3.setEmployeeMatricule("1789");
        p3.setEmployeeUsername("Sami Achour");
        p3.setPourcentage(20);
        p3.setTime("09:01:32");
        p3.setPointageTimes(timePointages3);

        pointageList.add(p);
        pointageList.add(p3);
        pointageList.add(p2);
        //pointageList.add(p);pointageList.add(p);
        showPointageList(pointageList);
        if (Utils.isNetworkAvailable(getActivity())) {
            Utils.showProgressDialog(progress, "TrackTime", "Chargement..", false);
            Glide.with(getActivity())
                    .load("https://img4.hostingpics.net/pics/5303753b7d6f60e2d450b899c322266fc6edfd.jpg")
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            Glide.with(getActivity())
                                    .load(R.drawable.errorloadimg)
                                    .into(imgUser);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(imgUser);
        } else {
            Utils.showAlert(getActivity(), "Erreur", "Verifier votre connexion internet !");
        }
        progress.dismiss();
        loadSpinnerDepartments();
        loadSpinnerSal();

    }

    public void showPointageList(List<Pointage> l) {
        pointageList = l;
        mLayoutManager = new LinearLayoutManager(getActivity());
        listPointage.setLayoutManager(mLayoutManager);
        PointageRecycleViewAdapter holidayAdapter = new PointageRecycleViewAdapter(getActivity(), pointageList);
        listPointage.setAdapter(holidayAdapter);
    }

    @Override
    public void onBackPressed() {

    }

    private void loadSpinnerDepartments() {
        ArrayList<String> arrayDepts = new ArrayList<>();
        arrayDepts.add(0, "Departements");
        arrayDepts.add("Mobile");
        arrayDepts.add("Web");
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item_txt, arrayDepts) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);

                TextView tv = (TextView) view;
                view.setPadding(10, 15, 10, 15);

                if (position == 0) {
                    tv.setTextColor(Color.GRAY);

                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item_txt);
        departSpinner.setAdapter(spinnerArrayAdapter);
        departSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text
                    Toast.makeText
                            (getActivity(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void loadSpinnerSal() {
        ArrayList<String> arraySal = new ArrayList<>();
        arraySal.add(0, "Tous les salariés");
        arraySal.add("Salarié Mobile");
        arraySal.add("Salarié Web");
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item_txt, arraySal) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);

                TextView tv = (TextView) view;
                view.setPadding(10, 15, 10, 15);

                if (position == 0) {
                    tv.setTextColor(Color.GRAY);

                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item_txt);
        salSpinner.setAdapter(spinnerArrayAdapter);
        salSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text
                    Toast.makeText
                            (getActivity(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.exportAuth: {
                Toast.makeText(getActivity(), "Export", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.filterAuth: {
                Toast.makeText(getActivity(), "Filter", Toast.LENGTH_SHORT).show();
                break;
            }
        }
    }
}

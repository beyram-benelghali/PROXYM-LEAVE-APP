package com.proxym.proxymleave.trackTimeAdmin.pointageList;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.proxym.proxymleave.R;
import com.proxym.proxymleave.common.Utils;
import com.proxym.proxymleave.models.entity.Pointage;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by beyram on 7/27/17.
 */

public class PointageRecycleViewAdapter extends RecyclerView.Adapter<PointageRecycleViewAdapter.ViewHolder> {

    List<Pointage> pointageList;
    View rowView;
    private Context mContext;

    public PointageRecycleViewAdapter(Context mContext, List<Pointage> pointageList) {
        this.pointageList = pointageList;
        this.mContext = mContext;
    }

    @Override
    public PointageRecycleViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pointage, parent, false);
        ViewHolder viewHolder = new ViewHolder(rowView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final PointageRecycleViewAdapter.ViewHolder holder, int position) {
        Pointage pointage = pointageList.get(position);
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date finalDate = null;
        Date startDate = null;
        try {
            finalDate = formatter.parse("2017-07-28 18:30:00");
            startDate = formatter.parse("2017-07-28 08:45:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.employeeMatricule.setText("Matricule : " + pointage.getEmployeeMatricule());
        holder.employeeName.setText(pointage.getEmployeeUsername());
        holder.timePointage.setText(pointage.getTime());
        if (pointage.getPointageTimes().get(0).compareTo(startDate) > 0) {
            Log.v("pointage", "pointage");
            holder.layoutborderStartTime.setBackgroundResource(R.drawable.border_pointage_retard);
        }

        if (pointage.getPointageTimes().size() == 4) {
            holder.startTime.setText(Utils.formatDateToTime(pointage.getPointageTimes().get(0)));
            holder.endTime.setText(Utils.formatDateToTime(pointage.getPointageTimes().get(3)));
            holder.pausefirstTime.setText(Utils.formatDateToTime(pointage.getPointageTimes().get(1)));
            holder.pausesecondTime.setText(Utils.formatDateToTime(pointage.getPointageTimes().get(2)));
            if (pointage.getPointageTimes().get(3).compareTo(finalDate) == 1) {
                LinearLayout.LayoutParams lay = (LinearLayout.LayoutParams) holder.endPointage.getLayoutParams();
                lay.weight = 1f;
                LinearLayout.LayoutParams layfree = (LinearLayout.LayoutParams) holder.freeSpace.getLayoutParams();
                layfree.weight = 0.58f;
                holder.layoutborderEndTime.setBackgroundResource(R.drawable.border_pointage_plus);
                LinearLayout.LayoutParams laystart = (LinearLayout.LayoutParams) holder.startPointage.getLayoutParams();
                laystart.weight = 1.8f;
                LinearLayout.LayoutParams laypause = (LinearLayout.LayoutParams) holder.firstPausePointage.getLayoutParams();
                laypause.weight = 0.8f;
            }
        } else if (pointage.getPointageTimes().size() < 4) {
            holder.startTime.setText(Utils.formatDateToTime(pointage.getPointageTimes().get(0)));
            holder.endTime.setText(Utils.formatDateToTime(pointage.getPointageTimes().get(2)));
            holder.pausefirstTime.setText(Utils.formatDateToTime(pointage.getPointageTimes().get(1)));
            holder.firstPausePointage.setVisibility(View.GONE);
            holder.layoutborderSecondTime.setBackgroundResource(R.drawable.border_pointage_impaire);

            //holder.pausesecondTime.setText(Utils.formatDateToTime(pointage.getPointageTimes().get(2)));
        }

        Glide.with(mContext)
                .load("https://img4.hostingpics.net/pics/5303753b7d6f60e2d450b899c322266fc6edfd.jpg")
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        Glide.with(mContext)
                                .load(R.drawable.errorloadimg)
                                .into(holder.imgEmployee);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(holder.imgEmployee);
    }

    @Override
    public int getItemCount() {
        return (null != pointageList ? pointageList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView employeeName;
        CircleImageView imgEmployee;
        TextView employeeMatricule;
        TextView timePointage;
        TextView endTime;
        TextView pausesecondTime;
        TextView pausefirstTime;
        TextView startTime;
        LinearLayout endPointage;
        LinearLayout layoutborderSecondTime, layoutborderEndTime, freeSpace, startPointage, layoutborderStartTime, firstPausePointage, SecondPausePointage;
        // SeekBar seekbarPointage;

        public ViewHolder(View view) {
            super(view);
            this.layoutborderSecondTime = (LinearLayout) view.findViewById(R.id.layoutborderSecondTime);
            this.SecondPausePointage = (LinearLayout) view.findViewById(R.id.SecondPausePointage);
            this.firstPausePointage = (LinearLayout) view.findViewById(R.id.firstPausePointage);
            this.layoutborderStartTime = (LinearLayout) view.findViewById(R.id.layoutborderStartTime);
            this.startPointage = (LinearLayout) view.findViewById(R.id.startPointage);
            this.layoutborderEndTime = (LinearLayout) view.findViewById(R.id.layoutborderEndTime);
            this.freeSpace = (LinearLayout) view.findViewById(R.id.freeSpace);
            this.endPointage = (LinearLayout) view.findViewById(R.id.endPointage);
            this.employeeName = (TextView) view.findViewById(R.id.employeeName);
            this.imgEmployee = (CircleImageView) view.findViewById(R.id.imgEmployee);
            this.employeeMatricule = (TextView) view.findViewById(R.id.employeeMatricule);
            this.timePointage = (TextView) view.findViewById(R.id.timePointage);
            this.endTime = (TextView) view.findViewById(R.id.endTime);
            this.pausesecondTime = (TextView) view.findViewById(R.id.pausesecondTime);
            this.pausefirstTime = (TextView) view.findViewById(R.id.pausefirstTime);
            this.startTime = (TextView) view.findViewById(R.id.startTime);

            // this.seekbarPointage = (SeekBar) view.findViewById(R.id.seekbarPointage);
        }
    }

}

package com.proxym.proxymleave.trackTimeAdmin.updateAuthorization;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.proxym.proxymleave.R;
import com.proxym.proxymleave.base.BaseActivity;
import com.proxym.proxymleave.base.BaseFragment;
import com.proxym.proxymleave.common.Utils;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class AuthorizationUpdateFragment extends BaseFragment implements View.OnTouchListener {

    View mView;
    @BindView(R.id.dateDemandeEditTXT)
    EditText dateDemandeEditTXT;
    @BindView(R.id.dateRecupEditTXT)
    EditText dateRecupEditTXT;
    @BindView(R.id.SpinnerStart)
    Spinner SpinnerStart;
    @BindView(R.id.SpinnerEnd)
    Spinner SpinnerEnd;
    @BindView(R.id.motifAuth)
    EditText motifAuth;
    ArrayList<String> arrayHours;
    int HourSelected = 1;
    ProgressDialog progress;
    @BindView(R.id.RequestImgUser)
    CircleImageView imgUser;

    public AuthorizationUpdateFragment() {
        // Required empty public constructor
    }

    @Override
    protected void initializePresenter() {

    }

    @Override
    public int getLayoutId() {
        return 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_authorization_update, container, false);
        super.setTitle("MISE A JOUR");
        ((BaseActivity) getActivity()).setOnBackPressedListener(this);
        return mView;
    }

    private void setupSpinnerEnd() {
        ArrayList<String> endHours = new ArrayList<>();
        for (int i = HourSelected; i < arrayHours.size(); i++) {
            endHours.add(arrayHours.get(i));
        }
        //endHours.addAll(HourSelected,arrayHours);
        endHours.add(0, "Jusqu'au");
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item_txt, endHours) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);

                TextView tv = (TextView) view;
                view.setPadding(10, 15, 10, 15);

                if (position == 0) {
                    tv.setTextColor(Color.GRAY);

                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item_txt);
        SpinnerEnd.setAdapter(spinnerArrayAdapter);
        SpinnerEnd.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text
                    Toast.makeText
                            (getActivity(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setupSpinnerStart() {
        arrayHours.add(0, "A partir de ");
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item_txt, arrayHours) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);

                TextView tv = (TextView) view;
                view.setPadding(10, 15, 10, 15);

                if (position == 0) {
                    tv.setTextColor(Color.GRAY);

                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item_txt);
        SpinnerStart.setAdapter(spinnerArrayAdapter);
        SpinnerStart.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text
                    HourSelected = position + 1;
                    Toast.makeText
                            (getActivity(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
                            .show();
                    setupSpinnerEnd();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progress = new ProgressDialog(getActivity());

        if (Utils.isNetworkAvailable(getActivity())) {
            Utils.showProgressDialog(progress, "TrackTime", "Chargement..", false);
            Glide.with(getActivity())
                    .load("https://img4.hostingpics.net/pics/5303753b7d6f60e2d450b899c322266fc6edfd.jpg")
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            Glide.with(getActivity())
                                    .load(R.drawable.errorloadimg)
                                    .into(imgUser);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(imgUser);
        } else {
            Utils.showAlert(getActivity(), "Erreur", "Verifier votre connexion internet !");
        }
        progress.dismiss();
        dateDemandeEditTXT.setOnTouchListener(this);
        dateDemandeEditTXT.setHint(Html.fromHtml(dateDemandeEditTXT.getHint().toString() + " <font color='" + getResources().getColor(R.color.redRequire) + "'>*" + "</font>"));
        dateRecupEditTXT.setHint(Html.fromHtml(dateRecupEditTXT.getHint().toString() + " <font color='" + getResources().getColor(R.color.redRequire) + "'>*" + "</font>"));
        dateRecupEditTXT.setOnTouchListener(this);
        arrayHours = new ArrayList<>();
        setHours();
        setupSpinnerStart();
        setupSpinnerEnd();
    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.dateDemandeEditTXT: {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int inType = dateDemandeEditTXT.getInputType(); // backup the input type
                    dateDemandeEditTXT.setInputType(InputType.TYPE_NULL); // disable soft input
                    dateDemandeEditTXT.onTouchEvent(event); // call native handler
                    dateDemandeEditTXT.setInputType(inType);
                    DatePickerDialog.OnDateSetListener dpd = new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear,
                                              int dayOfMonth) {
                            int s = monthOfYear + 1;
                            String a = dayOfMonth + "/" + s + "/" + year;
                            dateDemandeEditTXT.setText("" + a);
                        }
                    };
                    Calendar c = Calendar.getInstance();
                    int year = c.get(Calendar.YEAR);
                    int month = c.get(Calendar.MONTH);
                    int day = c.get(Calendar.DAY_OF_MONTH);
                    DatePickerDialog d = new DatePickerDialog(getActivity(), dpd, year, month, day);
                    d.setTitle(null);
                    d.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    d.show();
                }
                break;
            }
            case R.id.dateRecupEditTXT: {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int inType = dateRecupEditTXT.getInputType(); // backup the input type
                    dateRecupEditTXT.setInputType(InputType.TYPE_NULL); // disable soft input
                    dateRecupEditTXT.onTouchEvent(event); // call native handler
                    dateRecupEditTXT.setInputType(inType);
                    DatePickerDialog.OnDateSetListener dpd = new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear,
                                              int dayOfMonth) {
                            int s = monthOfYear + 1;
                            String a = dayOfMonth + "/" + s + "/" + year;
                            dateRecupEditTXT.setText("" + a);
                        }
                    };
                    Calendar c = Calendar.getInstance();
                    int year = c.get(Calendar.YEAR);
                    int month = c.get(Calendar.MONTH);
                    int day = c.get(Calendar.DAY_OF_MONTH);
                    DatePickerDialog d = new DatePickerDialog(getActivity(), dpd, year, month, day);
                    d.setTitle(null);
                    d.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    d.show();
                }
                break;
            }
        }
        return true;
    }

    void setHours() {

        arrayHours.add("9h");
        arrayHours.add("10h");
        arrayHours.add("11h");
        arrayHours.add("12h");
        arrayHours.add("13h");
        arrayHours.add("14h");
        arrayHours.add("15h");
        arrayHours.add("16h");
        arrayHours.add("17h");
    }

}

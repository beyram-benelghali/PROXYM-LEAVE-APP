package com.proxym.proxymleave.trackTimeAdmin.leaveList;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.proxym.proxymleave.R;
import com.proxym.proxymleave.base.BaseActivity;
import com.proxym.proxymleave.base.BaseFragment;
import com.proxym.proxymleave.common.Utils;
import com.proxym.proxymleave.models.entity.Transaction;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListCongesFragment extends BaseFragment implements View.OnClickListener {

    @BindView(R.id.listCongee)
    RecyclerView listCongee;
    List<Transaction> transactionList;
    LinearLayoutManager mLayoutManager;
    @BindView(R.id.RequestImgUser)
    CircleImageView imgUser;
    @BindView(R.id.etatSpinner)
    Spinner etatSpinner;
    @BindView(R.id.salSpinner)
    Spinner salSpinner;
    @BindView(R.id.yearSpinner)
    Spinner yearSpinner;
    @BindView(R.id.departSpinner)
    Spinner departSpinner;
    @BindView(R.id.exportAuth)
    Button exportAuth;
    @BindView(R.id.filterAuth)
    Button filterAuth;
    private ProgressDialog progress;

    public ListCongesFragment() {
        // Required empty public constructor
    }

    @Override
    protected void initializePresenter() {

    }

    @Override
    public int getLayoutId() {
        return 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_conges, container, false);
        super.setTitle("CONGÉS");
        progress = new ProgressDialog(getActivity());
        ((BaseActivity) getActivity()).setOnBackPressedListener(this);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listCongee.setHasFixedSize(true);
        filterAuth.setOnClickListener(this);
        exportAuth.setOnClickListener(this);
        transactionList = new ArrayList<>();
        Transaction transaction = new Transaction();
        transaction.setBeginDate("11/02/2017");
        transaction.setEndDate("12/02/2017");
        transaction.setTotalDays(1);
        transaction.setCategoryName("Congée de maladie");
        transaction.setEmployeeUsername("Mohamed Ali");
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        transactionList.add(transaction);
        showTransList(transactionList);
        if (Utils.isNetworkAvailable(getActivity())) {
            Utils.showProgressDialog(progress, "TrackTime", "Chargement..", false);
            Glide.with(getActivity())
                    .load("https://img4.hostingpics.net/pics/5303753b7d6f60e2d450b899c322266fc6edfd.jpg")
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            Glide.with(getActivity())
                                    .load(R.drawable.errorloadimg)
                                    .into(imgUser);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(imgUser);
        } else {
            Utils.showAlert(getActivity(), "Erreur", "Verifier votre connexion internet !");
        }
        loadSpinnerDepartments();
        loadSpinnerYear();
        loadSpinnerState();
        loadSpinnerSal();
        progress.dismiss();
    }

    @Override
    public void onBackPressed() {

    }

    public void showTransList(List<Transaction> l) {
        transactionList = l;
        mLayoutManager = new LinearLayoutManager(getActivity());
        listCongee.setLayoutManager(mLayoutManager);
        Log.v("HERE", "HERE");
        CongeRecycleViewAdapter holidayAdapter = new CongeRecycleViewAdapter(getActivity(), transactionList);
        listCongee.setAdapter(holidayAdapter);
    }

    private void loadSpinnerYear() {
        ArrayList<String> arrayYears = new ArrayList<>();
        arrayYears.add(0, "Toutes les années");
        arrayYears.add("2017");
        arrayYears.add("2016");
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item_txt, arrayYears) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);

                TextView tv = (TextView) view;
                view.setPadding(10, 15, 10, 15);

                if (position == 0) {
                    tv.setTextColor(Color.GRAY);

                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item_txt);
        yearSpinner.setAdapter(spinnerArrayAdapter);
        yearSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text
                    Toast.makeText
                            (getActivity(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void loadSpinnerSal() {
        ArrayList<String> arraySal = new ArrayList<>();
        arraySal.add(0, "Tous les salariés");
        arraySal.add("Salarié Mobile");
        arraySal.add("Salarié Web");
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item_txt, arraySal) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);

                TextView tv = (TextView) view;
                view.setPadding(10, 15, 10, 15);

                if (position == 0) {
                    tv.setTextColor(Color.GRAY);

                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item_txt);
        salSpinner.setAdapter(spinnerArrayAdapter);
        salSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text
                    Toast.makeText
                            (getActivity(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void loadSpinnerState() {
        ArrayList<String> arrayState = new ArrayList<>();
        arrayState.add(0, "Etat");
        arrayState.add("En attente");
        arrayState.add("Accepté");
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item_txt, arrayState) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);

                TextView tv = (TextView) view;
                view.setPadding(10, 15, 10, 15);

                if (position == 0) {
                    tv.setTextColor(Color.GRAY);

                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item_txt);
        etatSpinner.setAdapter(spinnerArrayAdapter);
        etatSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text
                    Toast.makeText
                            (getActivity(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void loadSpinnerDepartments() {
        ArrayList<String> arrayDepts = new ArrayList<>();
        arrayDepts.add(0, "Departements");
        arrayDepts.add("Mobile");
        arrayDepts.add("Web");
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item_txt, arrayDepts) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);

                TextView tv = (TextView) view;
                view.setPadding(10, 15, 10, 15);

                if (position == 0) {
                    tv.setTextColor(Color.GRAY);

                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item_txt);
        departSpinner.setAdapter(spinnerArrayAdapter);
        departSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text
                    Toast.makeText
                            (getActivity(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.exportAuth: {
                Toast.makeText(getActivity(), "Export", Toast.LENGTH_SHORT).show();
                break;
            }
            case R.id.filterAuth: {
                Toast.makeText(getActivity(), "Filter", Toast.LENGTH_SHORT).show();
                break;
            }
        }
    }
}

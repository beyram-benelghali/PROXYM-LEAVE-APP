package com.proxym.proxymleave.trackTimeAdmin.autorisationList;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.proxym.proxymleave.R;
import com.proxym.proxymleave.models.entity.Authorisation;
import com.proxym.proxymleave.trackTimeAdmin.updateAuthorization.AuthorizationUpdateFragment;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by beyram on 7/26/17.
 */

public class AutorisationRecycleViewAdapter extends RecyclerSwipeAdapter<AutorisationRecycleViewAdapter.ViewHolder> {

    List<Authorisation> listAuths;
    View rowView;
    private Context mContext;

    public AutorisationRecycleViewAdapter(Context mContext, List<Authorisation> listAuths) {
        this.listAuths = listAuths;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_autorisation, parent, false);
        ViewHolder viewHolder = new ViewHolder(rowView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Authorisation authorisation = listAuths.get(position);
        holder.dateDemande.setText(authorisation.getRequestDate());
        holder.etatDemande.setText("En attente");
        holder.dureeAuth.setText("Durée : " + (authorisation.getEndAuth() - authorisation.getStartAuth()));
        holder.RecupAuth.setText("Recuperation : " + authorisation.getRecupDate());
        holder.dateAuth.setText(authorisation.getAuthDate() + " du " + authorisation.getStartAuth() + " à " + authorisation.getEndAuth());
        holder.employeeName.setText(authorisation.getEmployeeUsername());
        Glide.with(mContext)
                .load("https://img4.hostingpics.net/pics/5303753b7d6f60e2d450b899c322266fc6edfd.jpg")
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        Glide.with(mContext)
                                .load(R.drawable.errorloadimg)
                                .into(holder.imgEmployee);
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        return false;
                    }
                })
                .into(holder.imgEmployee);

        holder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {

            @Override
            public void onOpen(SwipeLayout layout) {
                mItemManger.closeAllExcept(layout);
            }

        });

        mItemManger.bindView(holder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return (null != listAuths ? listAuths.size() : 0);
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipeAuth;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        LinearLayout linearLayoutRefuse, linearLayoutAccept, linearLayoutEdit;
        SwipeLayout swipeLayout;
        TextView dateDemande;
        TextView etatDemande;
        TextView dureeAuth;
        TextView dateAuth;
        TextView RecupAuth;
        TextView employeeName;
        CircleImageView imgEmployee;

        public ViewHolder(View view) {
            super(view);
            this.swipeLayout = (SwipeLayout) view.findViewById(R.id.swipeAuth);
            this.etatDemande = (TextView) view.findViewById(R.id.etatDemande);
            this.dateDemande = (TextView) view.findViewById(R.id.dateDemande);
            this.dureeAuth = (TextView) view.findViewById(R.id.dureeAuth);
            this.dateAuth = (TextView) view.findViewById(R.id.dateAuth);
            this.RecupAuth = (TextView) view.findViewById(R.id.RecupAuth);
            this.employeeName = (TextView) view.findViewById(R.id.employeeName);
            this.imgEmployee = (CircleImageView) view.findViewById(R.id.imgEmployee);
            this.linearLayoutRefuse = (LinearLayout) view.findViewById(R.id.linearLayoutRefuse);
            this.linearLayoutAccept = (LinearLayout) view.findViewById(R.id.linearLayoutAccept);
            this.linearLayoutEdit = (LinearLayout) view.findViewById(R.id.linearLayoutEdit);
            linearLayoutEdit.setOnClickListener(this);
            linearLayoutAccept.setOnClickListener(this);
            linearLayoutRefuse.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.linearLayoutRefuse: {
                    Toast.makeText(mContext, "Refuse", Toast.LENGTH_SHORT).show();
                    break;
                }
                case R.id.linearLayoutEdit: {
                    Toast.makeText(mContext, "AuthorizationUpdate", Toast.LENGTH_SHORT).show();
                    AuthorizationUpdateFragment fragment = new AuthorizationUpdateFragment();
                    android.support.v4.app.FragmentTransaction transaction = ((FragmentActivity) mContext).getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.container, fragment, "AuthorizationUpdateFragment");
                    transaction.addToBackStack(null);
                    transaction.commit();
                    break;
                }
                case R.id.linearLayoutAccept: {
                    Toast.makeText(mContext, "Accept", Toast.LENGTH_SHORT).show();
                    break;
                }
            }
        }
    }
}

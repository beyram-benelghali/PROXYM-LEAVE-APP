package com.proxym.proxymleave.trackTimeEmployee.holiday;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.proxym.proxymleave.R;
import com.proxym.proxymleave.base.BaseActivity;
import com.proxym.proxymleave.base.BaseFragment;
import com.proxym.proxymleave.common.Utils;
import com.proxym.proxymleave.models.entity.Holiday;
import com.proxym.proxymleave.models.manager.UserManager;
import com.proxym.proxymleave.trackTimeEmployee.dashboard.DashboardFragment;
import com.proxym.proxymleave.trackTimeEmployee.holiday.event.HolidayEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class HolidayFragment extends BaseFragment implements IHoliday_MVP.ViewOps {

    @BindView(R.id.listHoliday)
    RecyclerView listViewHoliday;
    List<Holiday> holidayList;
    LinearLayoutManager mLayoutManager;
    private IHoliday_MVP.PresenterViewOps mPresenter;
    private EventBus bus = EventBus.getDefault();
    private ProgressDialog progress;
    private FirebaseAnalytics mFirebaseAnalytics;

    public HolidayFragment() {
        // Required empty public constructor
    }

    /**
     * SETUP MVP Design Pattern
     */
    @Override
    protected void initializePresenter() {
        HolidayPresenter presenter = new HolidayPresenter(this);
        HolidayModel model = new HolidayModel(presenter);
        presenter.setModel(model);
        mPresenter = presenter;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_holiday;
    }

    /**
     * return View and handle BackButton event
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_holiday, container, false);
        super.setTitle("JOURS FERIÉS");
        progress = new ProgressDialog(getActivity());
        ((BaseActivity) getActivity()).setOnBackPressedListener(this);
        return view;
    }

    /**
     * Post HolidayEvent For Presenter to load Holiday list if isNetworkAvailable
     *
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listViewHoliday.setHasFixedSize(true);
        if (Utils.isNetworkAvailable(getActivity())) {
            Utils.showProgressDialog(progress, "TrackTime", "Chargement des jours feriés..", false);
            bus.post(new HolidayEvent(holidayList));
        } else {
            Utils.showAlert(getActivity(), "Erreur", "Verifier votre connexion internet !");
        }

        // TrackTime_Holiday_Fragment Firebase Analytics Event
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        mFirebaseAnalytics.setUserId(UserManager.getUsername());
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, UserManager.getCurrentUser().getEmail());
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, UserManager.getCurrentUser().getDisplayName());
        mFirebaseAnalytics.logEvent("TrackTime_Holiday", bundle);
    }

    @Override
    public Context getActivityContext() {
        return getActivity();
    }

    /**
     * dismiss ProgressDialog after loading holidays
     */
    @Override
    public void hideProgress() {
        progress.dismiss();
    }

    /**
     * Display RecycleView Holiday
     *
     * @param l
     */
    @Override
    public void showHolidayList(List<Holiday> l) {
        holidayList = l;
        mLayoutManager = new LinearLayoutManager(getActivity());
        listViewHoliday.setLayoutManager(mLayoutManager);
        HolidayRecycleViewAdapter holidayAdapter = new HolidayRecycleViewAdapter(getActivity(), holidayList);
        listViewHoliday.setAdapter(holidayAdapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    /**
     * Unregister from eventBus
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.unregisterEventBus();
    }

    /**
     * OnCLick backButton event
     */
    @Override
    public void onBackPressed() {
        DashboardFragment dashboardFragment = new DashboardFragment();
        android.support.v4.app.FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, dashboardFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}

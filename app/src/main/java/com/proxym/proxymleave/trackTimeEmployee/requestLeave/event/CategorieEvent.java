package com.proxym.proxymleave.trackTimeEmployee.requestLeave.event;

import com.proxym.proxymleave.models.entity.Category;

import java.util.List;

/**
 * Created by beyram on 06/07/17.
 */

/**
 * EventBus Event to load categories
 */
public class CategorieEvent {

    List<Category> categories;

    public CategorieEvent(List<Category> categories) {
        this.categories = categories;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}

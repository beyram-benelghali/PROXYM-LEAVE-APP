package com.proxym.proxymleave.trackTimeEmployee.requestLeave.event;

import com.proxym.proxymleave.models.entity.Category;

import java.util.List;

/**
 * Created by beyram on 06/07/17.
 */

/**
 * EventBus event for returning categories from server
 */
public class CategoryListEvent {

    private List<Category> categoryList;
    private boolean success = true;

    public CategoryListEvent(List<Category> categoryList) {
        this.categoryList = categoryList;
        this.success = true;
    }

    public CategoryListEvent(boolean success) {
        this.success = success;
    }

    public List<Category> getHolidayList() {
        return categoryList;
    }

    public void setHolidayList(List<Category> holidayList) {
        this.categoryList = holidayList;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}

package com.proxym.proxymleave.trackTimeEmployee.requestLeave;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.proxym.proxymleave.common.Utils;
import com.proxym.proxymleave.models.manager.UserManager;
import com.proxym.proxymleave.trackTimeEmployee.requestLeave.event.CategorieEvent;
import com.proxym.proxymleave.trackTimeEmployee.requestLeave.event.CategoryListEvent;
import com.proxym.proxymleave.trackTimeEmployee.requestLeave.event.RequestEvent;
import com.proxym.proxymleave.trackTimeEmployee.requestLeave.event.ResponseRequestEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.ref.WeakReference;

/**
 * Created by beyram on 06/07/17.
 */

public class RequestLeavePresenter implements IRequestLeave_MVP.PresenterModelOps, IRequestLeave_MVP.PresenterViewOps {

    // View reference
    private WeakReference<IRequestLeave_MVP.ViewOps> mView;
    // Model reference
    private IRequestLeave_MVP.ModelOps mModel;
    private Context context;
    private EventBus bus = EventBus.getDefault();
    private FirebaseAnalytics mFirebaseAnalytics;

    /**
     * Subscribe for CategoryList event - Response Request Event  eventBus
     *
     * @param view
     */
    public RequestLeavePresenter(IRequestLeave_MVP.ViewOps view) {
        mView = new WeakReference<>(view);
        context = mView.get().getActivityContext();
        bus.register(this);
    }

    /**
     * set model for Presenter
     *
     * @param model
     */
    public void setModel(IRequestLeave_MVP.ModelOps model) {
        mModel = model;
    }

    /**
     * get View from HolidayFragment
     *
     * @return
     * @throws NullPointerException
     */
    private IRequestLeave_MVP.ViewOps getView() throws NullPointerException {
        if (mView != null) {
            return mView.get();
        } else {
            throw new NullPointerException("View is unavailable");
        }
    }

    /**
     * set View for HolidayPresenter
     *
     * @param view
     */
    @Override
    public void setView(IRequestLeave_MVP.ViewOps view) {
        mView = new WeakReference<>(view);
    }

    /**
     * Unregister eventBus after loading holidays
     */
    @Override
    public void unregisterEventBus() {
        bus.unregister(this);
    }

    /**
     * Methode for hanling CategorieEvent from View- eventBus
     *
     * @param l
     */
    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void getCategories(CategorieEvent l) {
        mModel.getCategories();
    }

    /**
     * Methode for handling CategoryListEvent after loading categories
     *
     * @param l
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getCategoryList(CategoryListEvent l) {
        getView().hideProgress();
        getView().loadCategories(l.getHolidayList());
    }

    /**
     * Methode for hanling RequestEvent from View- eventBus
     *
     * @param l
     */
    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void saveTransaction(RequestEvent l) {
        String dateDebut = l.getDateDebut();
        String dateFin = l.getDateFin();
        int categorie = l.getCategorie();
        String motif = l.getMotif();
        int beginOnAfternoon = l.getBeginOnAfternoon();
        int endOnMorning = l.getEndOnMorning();
        mModel.saveTransactionRequest(dateDebut, dateFin, categorie, motif, endOnMorning, beginOnAfternoon);
    }

    /**
     * Methode for handling ResponseRequestEvent after save transaction request from server - eventBus
     *
     * @param l
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void saveTransactionState(final ResponseRequestEvent l) {
        getView().hideProgress();
        if (l.isSuccess()) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!l.isSuccess()) {
                        Utils.showAlert((Activity) context, "Error ", "Le solde de congés restant n'est pas suffisant pour passer cette demande.");
                    } else {
                        Utils.showAlert((Activity) context, "Success ", "La demande a été enregistrée avec succès.");
                        // TrackTime_RequestLeave_Fragment Firebase Analytics Event
                        mFirebaseAnalytics = FirebaseAnalytics.getInstance((Activity) context);
                        mFirebaseAnalytics.setUserId(UserManager.getUsername());
                        Bundle bundle = new Bundle();
                        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, UserManager.getCurrentUser().getEmail());
                        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, UserManager.getCurrentUser().getDisplayName());
                        mFirebaseAnalytics.logEvent("TrackTime_RequestLeave", bundle);
                    }
                }
            }, 500);
        } else {
            Utils.showAlert((Activity) context, "Error ", "Server is down !");
        }

    }

}

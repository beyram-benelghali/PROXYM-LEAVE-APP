package com.proxym.proxymleave.trackTimeEmployee.holiday;

import android.content.Context;

import com.proxym.proxymleave.models.entity.Holiday;

import java.util.List;

/**
 * Created by beyram on 06/07/17.
 */

public interface IHoliday_MVP {

    /**
     * View methods available to Presenter.
     */
    interface ViewOps {
        Context getActivityContext();

        void hideProgress();

        void showHolidayList(List<Holiday> l);
    }

    /**
     * Presenter Operations offered to View
     */
    interface PresenterViewOps {
        void setView(ViewOps view);

        void unregisterEventBus();
    }

    /**
     * Presenter Operations offered to Model
     */
    interface PresenterModelOps {
    }

    /**
     * Models Operations offered to Presenter
     */
    interface ModelOps {
        void getHolidays();
    }
}

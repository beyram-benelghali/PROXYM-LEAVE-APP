package com.proxym.proxymleave.trackTimeEmployee.requestLeave;

import android.util.Log;

import com.proxym.proxymleave.models.api.IProxymFake;
import com.proxym.proxymleave.models.api.RestClientFake;
import com.proxym.proxymleave.models.api.response.CategoryResponse;
import com.proxym.proxymleave.models.entity.Category;
import com.proxym.proxymleave.trackTimeEmployee.requestLeave.event.CategoryListEvent;
import com.proxym.proxymleave.trackTimeEmployee.requestLeave.event.ResponseRequestEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by beyram on 06/07/17.
 */

public class RequestLeaveModel implements IRequestLeave_MVP.ModelOps {

    IRequestLeave_MVP.PresenterModelOps mPresenter;
    IProxymFake service;
    private EventBus bus = EventBus.getDefault();

    public RequestLeaveModel(IRequestLeave_MVP.PresenterModelOps presenter) {
        this.mPresenter = presenter;
        service = RestClientFake.getInstance().getProxymLeaveAPI();
    }

    /**
     * Save Transaction Request
     *
     * @param dateDebut
     * @param dateFin
     * @param categorie
     * @param motif
     * @param endOnMorning
     * @param beginOnAfternoon
     */
    @Override
    public void saveTransactionRequest(String dateDebut, String dateFin, int categorie, String motif, int endOnMorning, int beginOnAfternoon) {
        Log.v("HERE", "HERE");
        Call<CategoryResponse> call = service.createTransaction("JSESSIONID"/*,dateDebut,dateFin,beginOnAfternoon,endOnMorning,motif,categorie*/);
        call.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                Log.v("BODY", response.body().toString());
                Log.v("Success", response.body().isSuccess() + "");
                bus.post(new ResponseRequestEvent(response.body().isSuccess()));
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                t.printStackTrace();
                bus.post(new ResponseRequestEvent(false));
            }
        });
    }

    /**
     * Loading Category list from server
     */
    @Override
    public void getCategories() {
        Call<CategoryResponse> call = service.getCategories("JSESSIONID");
        call.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                if (response.body().isSuccess() == true) {
                    List<Category> a = response.body().getBody();
                    bus.post(new CategoryListEvent(a));
                }
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                t.printStackTrace();
                bus.post(new CategoryListEvent(false));
            }
        });
    }
}

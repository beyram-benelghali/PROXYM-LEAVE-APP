package com.proxym.proxymleave.trackTimeEmployee.requestLeave;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.kyleduo.switchbutton.SwitchButton;
import com.proxym.proxymleave.R;
import com.proxym.proxymleave.base.BaseActivity;
import com.proxym.proxymleave.base.BaseFragment;
import com.proxym.proxymleave.common.Utils;
import com.proxym.proxymleave.models.entity.Category;
import com.proxym.proxymleave.trackTimeEmployee.dashboard.DashboardFragment;
import com.proxym.proxymleave.trackTimeEmployee.requestLeave.event.CategorieEvent;
import com.proxym.proxymleave.trackTimeEmployee.requestLeave.event.RequestEvent;

import org.greenrobot.eventbus.EventBus;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class RequestLeaveFragment extends BaseFragment implements View.OnClickListener, View.OnTouchListener, IRequestLeave_MVP.ViewOps {

    @BindView(R.id.typeSpinner)
    Spinner tSpinner;
    @BindView(R.id.CategorieSpinner)
    Spinner categorieSpinner;
    @BindView(R.id.startDateEditText)
    EditText startDate;
    @BindView(R.id.motifRequest)
    EditText motifRequest;
    @BindView(R.id.endDateEditText)
    EditText endDate;
    List<Category> categories;
    @BindView(R.id.startDateSwitchButton)
    SwitchButton startDateSwitchButton;
    @BindView(R.id.endDateSwitchButton)
    SwitchButton endDateSwitchButton;
    @BindView(R.id.saveRequestButton)
    Button saveRequestButton;
    @BindView(R.id.cancelRequestButton)
    Button cancelRequestButton;
    @BindView(R.id.RequestImgUser)
    CircleImageView imgUser;
    private IRequestLeave_MVP.PresenterViewOps mPresenter;
    private EventBus bus = EventBus.getDefault();
    private ProgressDialog progress;

    public RequestLeaveFragment() {
        // Required empty public constructor
    }

    /**
     * SETUP MVP Design Pattern
     */
    @Override
    protected void initializePresenter() {
        RequestLeavePresenter presenter = new RequestLeavePresenter(this);
        RequestLeaveModel model = new RequestLeaveModel(presenter);
        presenter.setModel(model);
        mPresenter = presenter;
    }

    @Override
    public int getLayoutId() {
        return R.layout.fragment_request_leave;
    }

    /**
     * return View and handle BackButton event
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_request_leave, container, false);
        super.setTitle("NOUVELLE DEMANDE");
        ((BaseActivity) getActivity()).setOnBackPressedListener(this);
        return view;
    }

    /**
     * Post CategorieEvent For Presenter to load category list if isNetworkAvailable
     * Checking StartDate - End Date EditText
     * add red * for required fields
     *
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progress = new ProgressDialog(getActivity());

        if (Utils.isNetworkAvailable(getActivity())) {
            bus.post(new CategorieEvent(categories));
            Utils.showProgressDialog(progress, "TrackTime", "Chargement..", false);
            Glide.with(getActivity())
                    .load("https://img4.hostingpics.net/pics/5303753b7d6f60e2d450b899c322266fc6edfd.jpg")
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            Glide.with(getActivity())
                                    .load(R.drawable.errorloadimg)
                                    .into(imgUser);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(imgUser);
        } else {
            Utils.showAlert(getActivity(), "Erreur", "Verifier votre connexion internet !");
        }

        saveRequestButton.setOnClickListener(this);
        cancelRequestButton.setOnClickListener(this);
        startDate.setOnTouchListener(this);
        startDate.setHint(Html.fromHtml(startDate.getHint().toString() + " <font color='" + getResources().getColor(R.color.redRequire) + "'>*" + "</font>"));
        endDate.setOnTouchListener(this);
        endDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                String dateFin = s.toString();

                try {
                    String dateDebut = startDate.getText().toString();
                    Date dateD = formatter.parse(dateDebut);
                    Date dateF = formatter.parse(dateFin);
                    if (dateD.compareTo(dateF) == 0) {
                        endDateSwitchButton.setChecked(true);
                        endDateSwitchButton.setEnabled(false);
                    } else {
                        endDateSwitchButton.setChecked(false);
                        endDateSwitchButton.setEnabled(true);
                    }
                } catch (ParseException e) {
                    // Utils.showAlert(getActivity(), "Warning", "Saisir la date de début");
                    e.printStackTrace();
                }
            }
        });
        endDate.setHint(Html.fromHtml(endDate.getHint().toString() + " <font color='" + getResources().getColor(R.color.redRequire) + "'>*" + "</font>"));
        motifRequest.setHint(Html.fromHtml(motifRequest.getHint().toString() + " <font color='" + getResources().getColor(R.color.redRequire) + "'>*" + "</font>"));

    }

    /**
     * Handle Save - Cancel Button Click
     *
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.saveRequestButton: {
                if (Utils.isNetworkAvailable(getActivity())) {
                    if (checkFormRequest()) {
                        int beginOnAfternoon;
                        int endOnMorning;
                        if (startDateSwitchButton.isChecked()) {
                            beginOnAfternoon = 0;
                        } else {
                            beginOnAfternoon = 1;
                        }
                        if (endDateSwitchButton.isChecked()) {
                            endOnMorning = 0;
                        } else {
                            endOnMorning = 1;
                        }
                        String dateDebut = startDate.getText().toString();
                        String dateFin = endDate.getText().toString();
                        int categorie = ((Category) categorieSpinner.getSelectedItem()).getId();
                        String motif = motifRequest.getText().toString();
                        bus.post(new RequestEvent(dateDebut, dateFin, categorie, motif, endOnMorning, beginOnAfternoon));
                        Utils.showProgressDialog(progress, "TrackTime", "Creation en cours..", false);
                    } else {
                        Utils.showAlert(getActivity(), "Erreur", "Veuillez remplir tous les champs obligatoires !");
                    }
                } else {
                    Utils.showAlert(getActivity(), "Erreur", "Verifier votre connexion internet !");
                }

                break;
            }
            case R.id.cancelRequestButton: {
                startDate.setText(null);
                endDate.setText(null);
                motifRequest.setText(null);
                endDateSwitchButton.setChecked(false);
                startDateSwitchButton.setChecked(false);
                categorieSpinner.setSelection(0, true);
                tSpinner.setSelection(0, true);
                break;
            }
        }
    }

    /**
     * Check if Required Field are empty or not
     *
     * @return
     */
    private boolean checkFormRequest() {
        boolean safe = true;
        if (endDate.getText().toString().isEmpty() || startDate.getText().toString().isEmpty() ||
                motifRequest.getText().toString().isEmpty() || categorieSpinner.getSelectedItem().toString().equals("Catégorie *")) {
            safe = false;
        }
        return safe;
    }

    /**
     * Dispaly DatePickerDialog on touch startDateEditText or endDateEditText
     *
     * @param v
     * @param event
     * @return
     */
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (v.getId()) {
            case R.id.startDateEditText: {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int inType = startDate.getInputType(); // backup the input type
                    startDate.setInputType(InputType.TYPE_NULL); // disable soft input
                    startDate.onTouchEvent(event); // call native handler
                    startDate.setInputType(inType);
                    DatePickerDialog.OnDateSetListener dpd = new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int monthOfYear,
                                              int dayOfMonth) {
                            int s = monthOfYear + 1;
                            String a = dayOfMonth + "/" + s + "/" + year;
                            startDate.setText("" + a);
                        }
                    };
                    Calendar c = Calendar.getInstance();
                    int year = c.get(Calendar.YEAR);
                    int month = c.get(Calendar.MONTH);
                    int day = c.get(Calendar.DAY_OF_MONTH);
                    DatePickerDialog d = new DatePickerDialog(getActivity(), dpd, year, month, day);
                    d.setTitle(null);
                    d.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                    d.show();
                }
                break;
            }
            case R.id.endDateEditText: {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (startDate.getText().toString().isEmpty()) {
                        Log.v("startDateEmpty", "startDateEmpty");
                        Utils.showAlert(getActivity(), "Warning", "Saisir la date de début");
                    } else {
                        int inType = endDate.getInputType(); // backup the input type
                        endDate.setInputType(InputType.TYPE_NULL); // disable soft input
                        endDate.onTouchEvent(event); // call native handler
                        endDate.setInputType(inType); // restore input type
                        DatePickerDialog.OnDateSetListener dpd = new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                                  int dayOfMonth) {
                                int s = monthOfYear + 1;
                                String a = dayOfMonth + "/" + s + "/" + year;
                                endDate.setText("" + a);
                            }
                        };
                        Calendar c = Calendar.getInstance();
                        int year = c.get(Calendar.YEAR);
                        int month = c.get(Calendar.MONTH);
                        int day = c.get(Calendar.DAY_OF_MONTH);
                        DatePickerDialog d = new DatePickerDialog(getActivity(), dpd, year, month, day);
                        d.setTitle(null);
                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                        Date dateDeb = null;
                        try {
                            dateDeb = formatter.parse(startDate.getText().toString());
                            d.getDatePicker().setMinDate(dateDeb.getTime());

                        } catch (ParseException e) {
                            d.getDatePicker().setMinDate(System.currentTimeMillis());
                        }

                        d.show();
                    }
                }

                break;
            }
        }
        return true;
    }

    @Override
    public Context getActivityContext() {
        return getActivity();
    }

    /**
     * dismiss ProgressDialog after loading holidays
     */
    @Override
    public void hideProgress() {
        progress.dismiss();
    }

    /**
     * Display Category - demande Spinners
     */
    @Override
    public void loadCategories(List<Category> categoryList) {
        this.categories = categoryList;
        setUPCategorySpinner(categorieSpinner, categories);
        ArrayList<String> types = new ArrayList<>();
        types.add("Type Demande 1");
        types.add("Type Demande 2");
        types.add("Type Demande 3");
        setUPTypesSpinner(tSpinner, types);
    }

    /**
     * Setup Spinner - Type demande
     *
     * @param spinner
     * @param types
     */
    private void setUPTypesSpinner(Spinner spinner, ArrayList<String> types) {
        types.add(0, "Type de demande ");
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                getActivity(), R.layout.spinner_item_txt, types) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);

                TextView tv = (TextView) view;
                view.setPadding(10, 15, 10, 15);

                if (position == 0) {
                    tv.setTextColor(Color.GRAY);

                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item_txt);
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItemText = (String) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text
                    Toast.makeText
                            (getActivity(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * Setup Spinner - Category
     *
     * @param spinner
     * @param categories
     */
    public void setUPCategorySpinner(final Spinner spinner, List<Category> categories) {
        Category firstItem = new Category();
        Spannable wordtoSpan = new SpannableString("*");
        wordtoSpan.setSpan(new ForegroundColorSpan(Color.RED), 0,
                wordtoSpan.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        firstItem.setName("Catégorie " + wordtoSpan);
        categories.add(0, firstItem);
        // Initializing an ArrayAdapter
        final ArrayAdapter<Category> spinnerArrayAdapter = new ArrayAdapter<Category>(
                getActivity(), R.layout.spinner_item_txt, categories) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner
                    // First item will be use for hint
                    return false;
                } else {
                    return true;
                }
            }

            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);

                TextView tv = (TextView) view;
                view.setPadding(10, 15, 10, 15);

                if (position == 0) {
                    tv.setTextColor(Color.GRAY);

                } else {
                    tv.setTextColor(Color.BLACK);
                }
                return view;
            }
        };
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item_txt);
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Category selectedItemText = (Category) parent.getItemAtPosition(position);
                // If user change the default selection
                // First item is disable and it is used for hint
                if (position > 0) {
                    // Notify the selected item text
                    Toast.makeText
                            (getActivity(), "Selected : " + selectedItemText, Toast.LENGTH_SHORT)
                            .show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    /**
     * Unregister from eventBus
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresenter.unregisterEventBus();
    }

    /**
     * OnCLick backButton event
     */
    @Override
    public void onBackPressed() {
        DashboardFragment dashboardFragment = new DashboardFragment();
        android.support.v4.app.FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, dashboardFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}

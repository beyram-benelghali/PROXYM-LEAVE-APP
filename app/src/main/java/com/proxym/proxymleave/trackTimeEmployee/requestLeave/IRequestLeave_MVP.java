package com.proxym.proxymleave.trackTimeEmployee.requestLeave;

import android.content.Context;

import com.proxym.proxymleave.models.entity.Category;

import java.util.List;

/**
 * Created by beyram on 06/07/17.
 */

public interface IRequestLeave_MVP {

    /**
     * View methods available to Presenter.
     */
    interface ViewOps {
        Context getActivityContext();

        void hideProgress();

        void loadCategories(List<Category> categoryList);
    }

    /**
     * Presenter Operations offered to View
     */
    interface PresenterViewOps {
        void setView(ViewOps view);

        void unregisterEventBus();
    }

    /**
     * Presenter Operations offered to Model
     */
    interface PresenterModelOps {
    }

    /**
     * Models Operations offered to Presenter
     */
    interface ModelOps {
        void getCategories();

        void saveTransactionRequest(String dateDebut, String dateFin, int categorie, String motif, int endOnMorning, int beginOnAfternoon);
    }
}

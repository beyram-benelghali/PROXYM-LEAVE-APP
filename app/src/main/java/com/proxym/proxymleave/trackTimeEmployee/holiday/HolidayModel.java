package com.proxym.proxymleave.trackTimeEmployee.holiday;

import com.proxym.proxymleave.models.api.IProxymFake;
import com.proxym.proxymleave.models.api.RestClientFake;
import com.proxym.proxymleave.models.api.response.HolidayResponse;
import com.proxym.proxymleave.models.entity.Holiday;
import com.proxym.proxymleave.trackTimeEmployee.holiday.event.HolidayListEvent;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by beyram on 06/07/17.
 */

public class HolidayModel implements IHoliday_MVP.ModelOps {

    IHoliday_MVP.PresenterModelOps mPresenter;
    IProxymFake service;
    private EventBus bus = EventBus.getDefault();

    public HolidayModel(IHoliday_MVP.PresenterModelOps presenter) {
        this.mPresenter = presenter;
        service = RestClientFake.getInstance().getProxymLeaveAPI();
    }

    /**
     * Loading Holidays list from IProxymFake
     */
    @Override
    public void getHolidays() {
        Call<HolidayResponse> call = service.getHolydays("JSESSIONID");
        call.enqueue(new Callback<HolidayResponse>() {
            @Override
            public void onResponse(Call<HolidayResponse> call, Response<HolidayResponse> response) {
                if (response.body().isSuccess() == true) {
                    List<Holiday> a = response.body().getBody();
                    bus.post(new HolidayListEvent(a));
                } else {
                    bus.post(new HolidayListEvent(false));
                }

            }

            @Override
            public void onFailure(Call<HolidayResponse> call, Throwable t) {
                bus.post(new HolidayListEvent(false));
            }
        });
    }
}

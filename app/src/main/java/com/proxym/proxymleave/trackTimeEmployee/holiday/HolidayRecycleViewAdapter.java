package com.proxym.proxymleave.trackTimeEmployee.holiday;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.proxym.proxymleave.R;
import com.proxym.proxymleave.common.Utils;
import com.proxym.proxymleave.models.entity.Holiday;

import java.util.List;

/**
 * Created by beyram on 27/06/17.
 */

public class HolidayRecycleViewAdapter extends RecyclerView.Adapter<HolidayRecycleViewAdapter.ViewHolder> {

    List<Holiday> listHoliday;
    View rowView;
    private Context mContext;

    public HolidayRecycleViewAdapter(Context mContext, List<Holiday> listHoliday) {
        this.listHoliday = listHoliday;
        this.mContext = mContext;
    }

    /**
     * Prepare ViewHolder that can represent the items
     *
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.array_holiday_item_light, parent, false);
        ViewHolder viewHolder = new ViewHolder(rowView);
        return viewHolder;
    }

    /**
     * Binding Holiday Data in holder
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Holiday holiday = listHoliday.get(position);
        holder.holidayDate.setText(Utils.formatDate(holiday.getDateH()).toString());
        holder.holidayName.setText(holiday.getNomH());
        if (position % 2 != 1) {
            rowView.setBackgroundColor(ContextCompat.getColor(mContext, R.color.purpleBorderImg));
            rowView.getBackground().setAlpha(180);

        } else {
            holder.holidayName.setTextColor(ContextCompat.getColor(mContext, R.color.colortxtSpinner));
        }
    }

    @Override
    public int getItemCount() {
        return (null != listHoliday ? listHoliday.size() : 0);
    }

    /**
     * A ViewHolder describes Holiday item view
     */
    class ViewHolder extends RecyclerView.ViewHolder {
        TextView holidayName;
        TextView holidayDate;

        public ViewHolder(View view) {
            super(view);
            this.holidayDate = (TextView) view.findViewById(R.id.tvHolidayDate);
            this.holidayName = (TextView) view.findViewById(R.id.tvHolidayName);
        }
    }
}

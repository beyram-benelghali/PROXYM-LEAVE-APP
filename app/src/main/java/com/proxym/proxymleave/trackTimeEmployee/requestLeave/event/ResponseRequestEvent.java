package com.proxym.proxymleave.trackTimeEmployee.requestLeave.event;

/**
 * Created by beyram on 06/07/17.
 */

/**
 * EventBus event for returning server response if transaction is created or not
 */
public class ResponseRequestEvent {

    boolean success;

    public ResponseRequestEvent(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}

package com.proxym.proxymleave.trackTimeEmployee.holiday.event;

import com.proxym.proxymleave.models.entity.Holiday;

import java.util.List;

/**
 * Created by beyram on 06/07/17.
 */

/**
 * EventBus Event to load Holidays
 */
public class HolidayEvent {

    private List<Holiday> holidayList;

    public HolidayEvent(List<Holiday> holidayList) {
        this.holidayList = holidayList;
    }

    public List<Holiday> getHolidayList() {
        return holidayList;
    }

    public void setHolidayList(List<Holiday> holidayList) {
        this.holidayList = holidayList;
    }
}

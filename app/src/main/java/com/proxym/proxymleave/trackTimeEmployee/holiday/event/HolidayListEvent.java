package com.proxym.proxymleave.trackTimeEmployee.holiday.event;

import com.proxym.proxymleave.models.entity.Holiday;

import java.util.List;

/**
 * Created by beyram on 06/07/17.
 */

/**
 * EventBus event for returning holidays from server
 */
public class HolidayListEvent {

    private List<Holiday> holidayList;
    private boolean success = true;

    public HolidayListEvent(List<Holiday> holidayList) {
        this.holidayList = holidayList;
    }

    public HolidayListEvent(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<Holiday> getHolidayList() {
        return holidayList;
    }

    public void setHolidayList(List<Holiday> holidayList) {
        this.holidayList = holidayList;
    }

}

package com.proxym.proxymleave.trackTimeEmployee.requestLeave.event;

/**
 * Created by beyram on 06/07/17.
 */

/**
 * EventBus event for creating Transaction
 */
public class RequestEvent {

    String dateDebut;
    String dateFin;
    int categorie;
    String motif;
    int endOnMorning;
    int beginOnAfternoon;

    public RequestEvent() {
    }

    public RequestEvent(String dateDebut, String dateFin, int categorie, String motif, int endOnMorning, int beginOnAfternoon) {
        this.dateDebut = dateDebut;
        this.dateFin = dateFin;
        this.categorie = categorie;
        this.motif = motif;
        this.endOnMorning = endOnMorning;
        this.beginOnAfternoon = beginOnAfternoon;
    }

    public String getDateDebut() {
        return dateDebut;
    }

    public void setDateDebut(String dateDebut) {
        this.dateDebut = dateDebut;
    }

    public String getDateFin() {
        return dateFin;
    }

    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }

    public int getCategorie() {
        return categorie;
    }

    public void setCategorie(int categorie) {
        this.categorie = categorie;
    }

    public String getMotif() {
        return motif;
    }

    public void setMotif(String motif) {
        this.motif = motif;
    }

    public int getEndOnMorning() {
        return endOnMorning;
    }

    public void setEndOnMorning(int endOnMorning) {
        this.endOnMorning = endOnMorning;
    }

    public int getBeginOnAfternoon() {
        return beginOnAfternoon;
    }

    public void setBeginOnAfternoon(int beginOnAfternoon) {
        this.beginOnAfternoon = beginOnAfternoon;
    }
}

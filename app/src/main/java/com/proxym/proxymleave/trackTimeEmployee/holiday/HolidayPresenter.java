package com.proxym.proxymleave.trackTimeEmployee.holiday;

import android.app.Activity;
import android.content.Context;

import com.proxym.proxymleave.common.Utils;
import com.proxym.proxymleave.trackTimeEmployee.holiday.event.HolidayEvent;
import com.proxym.proxymleave.trackTimeEmployee.holiday.event.HolidayListEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.lang.ref.WeakReference;

/**
 * Created by beyram on 29/06/17.
 */

public class HolidayPresenter implements IHoliday_MVP.PresenterModelOps, IHoliday_MVP.PresenterViewOps {

    // View reference
    private WeakReference<IHoliday_MVP.ViewOps> mView;
    // Model reference
    private IHoliday_MVP.ModelOps mModel;
    private Context context;
    private EventBus bus = EventBus.getDefault();

    /**
     * Subscribe for HolidayEvent -  eventBus
     *
     * @param view
     */
    public HolidayPresenter(IHoliday_MVP.ViewOps view) {
        mView = new WeakReference<>(view);
        context = mView.get().getActivityContext();
        bus.register(this);
    }

    /**
     * set model for Presenter
     *
     * @param model
     */
    public void setModel(IHoliday_MVP.ModelOps model) {
        mModel = model;
    }

    /**
     * get View from HolidayFragment
     *
     * @return
     * @throws NullPointerException
     */
    private IHoliday_MVP.ViewOps getView() throws NullPointerException {
        if (mView != null) {
            return mView.get();
        } else {
            throw new NullPointerException("View is unavailable");
        }
    }

    /**
     * set View for HolidayPresenter
     *
     * @param view
     */
    @Override
    public void setView(IHoliday_MVP.ViewOps view) {
        mView = new WeakReference<>(view);
    }

    /**
     * Unregister eventBus after loading holidays
     */
    @Override
    public void unregisterEventBus() {
        bus.unregister(this);
    }

    /**
     * Methode for hanling HolidayEvent from View- eventBus
     *
     * @param l
     */
    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void getHolidays(HolidayEvent l) {
        mModel.getHolidays();
    }

    /**
     * Methode for handling HolidayListEvent after loading from server - eventBus
     *
     * @param l
     */
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void getHolidays(HolidayListEvent l) {
        getView().hideProgress();
        if (l.isSuccess()) {
            getView().showHolidayList(l.getHolidayList());
        } else {
            Utils.showAlert((Activity) context, "Error", "Server is down !");
        }
    }

}

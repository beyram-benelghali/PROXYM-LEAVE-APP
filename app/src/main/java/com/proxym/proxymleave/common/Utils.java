package com.proxym.proxymleave.common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by beyram on 23/06/17.
 */

public class Utils {

    /**
     * for formatting date
     *
     * @param date
     * @return
     */
    public static String formatDate(Date date) {
        return new SimpleDateFormat("MM-dd-yyyy").format(date);
    }

    public static String formatDateToTime(Date date) {
        return new SimpleDateFormat("HH:mm:ss").format(date);
    }

    /**
     * Show AlertDialog in View
     *
     * @param activity
     * @param title
     * @param message
     */
    public static void showAlert(Activity activity, String title, String message) {
        AlertDialogFragment.newInstance(title, message).showAlert(activity);
    }

    /**
     * Show ProgressDialog in View
     *
     * @param progress
     * @param title
     * @param message
     * @param cancelable
     */
    public static void showProgressDialog(ProgressDialog progress, String title, String message, boolean cancelable) {
        progress.setTitle(title);
        progress.setMessage(message);
        progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progress.setCancelable(cancelable);
        progress.show();
    }

    /**
     * CHECK IF DEVICE IS CONNECTED TO NETWORK ( Without internet access )
     *
     * @param context
     * @return
     */
    public static boolean isNetworkAvailable(Context context) {
        final ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * CHECK IF DEVICE IS CONNECTED TO INTERNET
     *
     * @param context
     * @return
     */
    public static int isInternetAvailable(Context context) {
        if (Utils.isNetworkAvailable(context)) {
            try {
                if (Runtime.getRuntime().exec("/system/bin/ping -c 1 -W 2 8.8.8.8").waitFor() == 0) {
                    return 0;
                } else {
                    return 2;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            return 1;
        } else {
            return 1;
        }
    }

    /**
     * Email Validation
     *
     * @param target
     * @return
     */
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

}

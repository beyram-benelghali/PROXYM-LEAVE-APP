package com.proxym.proxymleave.common;

/**
 * Created by beyram on 7/11/17.
 */

/**
 * Interface to Communicate between CheckInternetTask and LoginActivity
 */
public interface ICheckInternet {
    void successCheckConnexion();
}

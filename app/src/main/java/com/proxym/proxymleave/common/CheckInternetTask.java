package com.proxym.proxymleave.common;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by beyram on 7/11/17.
 */

/**
 * AsyncTask for checking internet connection
 */
public class CheckInternetTask extends AsyncTask<Void, Void, Boolean> {

    Context context;
    boolean connected = false;
    ProgressDialog progress;
    ICheckInternet iCheck;

    public CheckInternetTask(ICheckInternet iCheck, Context context, ProgressDialog progress) {
        this.iCheck = iCheck;
        this.progress = progress;
        this.context = context;
    }

    /**
     * ping google to check internet connection
     *
     * @param params
     * @return
     */
    @Override
    protected Boolean doInBackground(Void... params) {
        if (Utils.isNetworkAvailable(context)) {

            try {
                URL url = new URL("http://www.google.com/");
                HttpURLConnection urlc = (HttpURLConnection) url.openConnection();
                urlc.setRequestProperty("User-Agent", "test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(3000); // mTimeout is in seconds
                urlc.connect();
                if (urlc.getResponseCode() == 200) {
                    connected = true;
                } else {
                    connected = false;
                }
            } catch (IOException e) {
                Log.v("warning", "Error checking internet connection", e);
                connected = false;
            }
        }
        return connected;
    }

    @Override
    protected void onPreExecute() {
        Utils.showProgressDialog(progress, "TrackTime Login", "Connexion en cours..", false);
    }

    /**
     * if connected call successCheckConnexion in View that implements ICheckInternet (LoginActivity)
     *
     * @param aBoolean
     */
    @Override
    protected void onPostExecute(Boolean aBoolean) {
        if (connected) {
            iCheck.successCheckConnexion();
        } else {
            progress.dismiss();
            Utils.showAlert((Activity) context, "Erreur", "Verifier votre connexion internet !");
        }

    }
}

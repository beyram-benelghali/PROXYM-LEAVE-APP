package com.proxym.proxymleave.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

/**
 * Created by beyram on 7/13/17.
 */

public class InternetBReciever extends BroadcastReceiver {

    IInternetBReciever internetBReciever;
    public InternetBReciever(IInternetBReciever internetBReciever) {
        this.internetBReciever = internetBReciever;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
            switch (Utils.isInternetAvailable(context)) {
                case 0: {
                    internetBReciever.updateSnackBar(0);
                    break;
                }
                case 1: {
                    internetBReciever.updateSnackBar(1);
                    break;
                }
                case 2: {
                    internetBReciever.updateSnackBar(2);
                    break;
                }
            }
        }
    }
}

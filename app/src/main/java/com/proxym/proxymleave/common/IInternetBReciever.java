package com.proxym.proxymleave.common;

/**
 * Created by beyram on 7/13/17.
 */

/**
 * Interface to Communicate between Internet BroadCast reciever and MainActivity for updating snackBar state
 */
public interface IInternetBReciever {
    void updateSnackBar(int state);
}

package com.proxym.proxymleave.models.api;

import com.proxym.proxymleave.models.api.response.LoginResponse;
import com.proxym.proxymleave.models.api.response.LogoutResponse;
import com.proxym.proxymleave.models.entity.Category;
import com.proxym.proxymleave.models.entity.Holiday;
import com.proxym.proxymleave.models.entity.Transaction;
import com.proxym.proxymleave.models.entity.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by beyram on 28/06/17.
 */

public interface IProxymLeaveAPI {

    String ENDPOINT = "https://tracktime.proxym-group.net/";

    /**
     * Login Action
     */
    @FormUrlEncoded
    @POST("j_spring_security_check")
    Call<LoginResponse> login(@Field("j_username") String j_username,
                              @Field("j_password") String j_password,
                              @Field("ajax") boolean ajax);

    /**
     * Logout Action
     */
    @POST("service/logout")
    Call<LogoutResponse> logout();

    /**
     * Transaction Request Action
     */
    @POST("service/transaction/createTransaction")
    Call<Void> transactionRequest(@Header("JSESSIONID") String jSessionId,
                                  @Path("beginDate") String beginDate,
                                  @Path("endDate") String endDate,
                                  @Path("eventDate") String eventDate,
                                  @Path("beginOnAfternoon") boolean beginOnAfternoon,
                                  @Path("endOnMorning") boolean endOnMorning,
                                  @Path("motif") String motif,
                                  @Path("category") int category);

    /**
     * Transaction Update Action
     */
    @POST("service/transaction/updateTransaction")
    Call<Void> updateTransaction(@Header("JSESSIONID") String jSessionId,
                                 @Path("beginDate") String beginDate,
                                 @Path("endDate") String endDate,
                                 @Path("eventDate") String eventDate,
                                 @Path("beginOnAfternoon") boolean beginOnAfternoon,
                                 @Path("endOnMorning") boolean endOnMorning,
                                 @Path("motif") String motif,
                                 @Path("category") int category);

    /**
     * Cancel Transaction Request Action
     */
    @POST("service/transaction/cancelTransaction")
    Call<Void> cancelTransactionRequest(@Header("JSESSIONID") String jSessionId,
                                        @Path("id") int transactionId);

    /**
     * Reject Transaction Request Action
     */
    @POST("service/transaction/reject")
    Call<Void> rejectTransaction(@Header("JSESSIONID") String jSessionId,
                                 @Path("id") int transactionId);

    /**
     * Accept Transaction Request Action
     */
    @POST("service/transaction/accept")
    Call<Void> acceptTransaction(@Header("JSESSIONID") String jSessionId,
                                 @Path("id") int transactionId);

    /**
     * get Transaction Action
     */
    @POST("service/transaction/getTransaction")
    Call<Transaction> getTransaction(@Header("JSESSIONID") String jSessionId,
                                     @Path("id") int transactionId);

    /**
     * get All Transactions
     */
    @POST("service/transaction/listTransactions")
    Call<List<Transaction>> getAllTransaction(@Header("JSESSIONID") String jSessionId);

    /**
     * get All Category
     */
    @POST("service/transaction/getCategories")
    Call<List<Category>> getCategories(@Header("JSESSIONID") String jSessionId);

    /**
     * get All Years
     */
    @POST("service/transaction/getYears")
    Call<List<Integer>> getYears(@Header("JSESSIONID") String jSessionId);

    /**
     * get Recap
     */
    @POST("service/recap")
    Call<List<User>> getRecap(@Header("JSESSIONID") String jSessionId,
                              @Path("id") int employeeId
    );

    /**
     * get All Holidays
     */
    @POST("service/transaction/getHolydays")
    Call<List<Holiday>> getHolydays(@Header("JSESSIONID") String jSessionId);

}


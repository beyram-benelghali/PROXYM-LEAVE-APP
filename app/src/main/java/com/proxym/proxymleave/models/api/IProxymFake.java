package com.proxym.proxymleave.models.api;

import com.proxym.proxymleave.models.api.response.CategoryResponse;
import com.proxym.proxymleave.models.api.response.HolidayResponse;
import com.proxym.proxymleave.models.api.response.LoginResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by beyram on 05/07/17.
 */

/**
 * Fake Server
 */
public interface IProxymFake {

    String ENDPOINT = "https://api.myjson.com/bins/";

    @GET("p45nj")
    Call<LoginResponse> login();

    /**
     * get All Holidays
     */
    @GET("1b6xjb")
    Call<HolidayResponse> getHolydays(@Header("JSESSIONID") String jSessionId);

    /**
     * get All Category
     */
    @GET("13r0h3")
    Call<CategoryResponse> getCategories(@Header("JSESSIONID") String jSessionId);

    @GET("p6jav")
    Call<CategoryResponse> createTransaction(@Header("JSESSIONID") String jSessionId /*,
                                              @Path("beginDate") String beginDate ,
                                              @Path("endDate") String endDate ,
                                              @Path("beginOnAfternoon") int beginOnAfternoon ,
                                              @Path("endOnMorning") int endOnMorning ,
                                              @Path("motif") String motif ,
                                              @Path("category") int category*/
    );

}

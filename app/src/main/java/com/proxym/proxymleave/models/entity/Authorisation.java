package com.proxym.proxymleave.models.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by beyram on 7/26/17.
 */

public class Authorisation {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("requestDate")
    @Expose
    private String requestDate;

    @SerializedName("authDate")
    @Expose
    private String authDate;

    @SerializedName("startAuth")
    @Expose
    private int endAuth;

    @SerializedName("endAuth")
    @Expose
    private int startAuth;

    @SerializedName("recupDate")
    @Expose
    private String recupDate;

    @SerializedName("motif")
    @Expose
    private int motif;

    @SerializedName("status")
    @Expose
    private int status;

    @SerializedName("employeeId")
    @Expose
    private int employeeId;

    @SerializedName("employeeUsername")
    @Expose
    private String employeeUsername;

    public Authorisation() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public String getAuthDate() {
        return authDate;
    }

    public void setAuthDate(String authDate) {
        this.authDate = authDate;
    }

    public int getEndAuth() {
        return endAuth;
    }

    public void setEndAuth(int endAuth) {
        this.endAuth = endAuth;
    }

    public int getStartAuth() {
        return startAuth;
    }

    public void setStartAuth(int startAuth) {
        this.startAuth = startAuth;
    }

    public String getRecupDate() {
        return recupDate;
    }

    public void setRecupDate(String recupDate) {
        this.recupDate = recupDate;
    }

    public int getMotif() {
        return motif;
    }

    public void setMotif(int motif) {
        this.motif = motif;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeUsername() {
        return employeeUsername;
    }

    public void setEmployeeUsername(String employeeUsername) {
        this.employeeUsername = employeeUsername;
    }
}

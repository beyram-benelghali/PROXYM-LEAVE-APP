package com.proxym.proxymleave.models.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by beyram on 28/06/17.
 */

public class Transaction {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("requestDate")
    @Expose
    private String requestDate;

    @SerializedName("beginDate")
    @Expose
    private String beginDate;

    @SerializedName("endDate")
    @Expose
    private String endDate;

    @SerializedName("eventDate")
    @Expose
    private String eventDate;

    @SerializedName("beginOnAfternoon")
    @Expose
    private int beginOnAfternoon;

    @SerializedName("endOnMorning")
    @Expose
    private int endOnMorning;

    @SerializedName("totalDays")
    @Expose
    private int totalDays;

    @SerializedName("employeeId")
    @Expose
    private int employeeId;

    @SerializedName("employeeUsername")
    @Expose
    private String employeeUsername;

    @SerializedName("categoryId")
    @Expose
    private int categoryId;

    @SerializedName("categoryName")
    @Expose
    private String categoryName;

    @SerializedName("isEvent")
    @Expose
    private boolean isEvent;

    @SerializedName("event")
    @Expose
    private String event;

    public Transaction() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }

    public String getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(String beginDate) {
        this.beginDate = beginDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public int isBeginOnAfternoon() {
        return beginOnAfternoon;
    }

    public void setBeginOnAfternoon(int beginOnAfternoon) {
        this.beginOnAfternoon = beginOnAfternoon;
    }

    public int isEndOnMorning() {
        return endOnMorning;
    }

    public void setEndOnMorning(int endOnMorning) {
        this.endOnMorning = endOnMorning;
    }

    public int getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(int totalDays) {
        this.totalDays = totalDays;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeUsername() {
        return employeeUsername;
    }

    public void setEmployeeUsername(String employeeUsername) {
        this.employeeUsername = employeeUsername;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public boolean isEvent() {
        return isEvent;
    }

    public void setEvent(boolean event) {
        isEvent = event;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }
}

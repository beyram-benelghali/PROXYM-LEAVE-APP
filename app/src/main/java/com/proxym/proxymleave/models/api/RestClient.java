package com.proxym.proxymleave.models.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by beyram on 28/06/17.
 */

public class RestClient {

    private static RestClient instance = null;
    Retrofit retrofit;
    private IProxymLeaveAPI apiService;

    private RestClient() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();
        OkHttpClient client = new OkHttpClient.Builder()
                .cookieJar(new LoginCookie())
                .build();
        retrofit = new Retrofit.Builder()
                .baseUrl(IProxymLeaveAPI.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        apiService = retrofit.create(IProxymLeaveAPI.class);
    }

    public static RestClient getInstance() {
        if (instance == null) {
            instance = new RestClient();
        }
        return instance;
    }

    public IProxymLeaveAPI getProxymLeaveAPI() {
        return apiService;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

}

package com.proxym.proxymleave.models.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by beyram on 28/06/17.
 */

public class Category {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("code")
    @Expose
    private String code;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("sign")
    @Expose
    private String sign;

    @SerializedName("nbrDaysAuthorised")
    @Expose
    private int nbrDaysAuthorised;

    @SerializedName("event")
    @Expose
    private boolean event;

    public Category() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public int getNbrDaysAuthorised() {
        return nbrDaysAuthorised;
    }

    public void setNbrDaysAuthorised(int nbrDaysAuthorised) {
        this.nbrDaysAuthorised = nbrDaysAuthorised;
    }

    public boolean isEvent() {
        return event;
    }

    public void setEvent(boolean event) {
        this.event = event;
    }

    @Override
    public String toString() {
        return name;
    }
}

package com.proxym.proxymleave.models.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by beyram on 23/06/17.
 */

public class Holiday {
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("name")
    @Expose
    private String nomH;

    @SerializedName("date")
    @Expose
    private Date dateH;

    public Holiday(String nomH, Date dateH) {
        this.nomH = nomH;
        this.dateH = dateH;
    }

    public Holiday() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomH() {
        return nomH;
    }

    public void setNomH(String nomH) {
        this.nomH = nomH;
    }

    public Date getDateH() {
        return dateH;
    }

    public void setDateH(Date dateH) {
        this.dateH = dateH;
    }
}

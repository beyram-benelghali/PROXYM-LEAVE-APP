package com.proxym.proxymleave.models.manager;

import com.proxym.proxymleave.models.entity.User;

/**
 * Created by beyram on 28/06/17.
 */

/**
 * UserManager to manage  User connected DATA
 */
public class UserManager {

    static String jSessionId;
    static String username;
    static String password;
    static User currentUser;

    public UserManager() {
    }

    public static String getJessionId() {
        return jSessionId;
    }

    public static void setJessionId(String jSessionIdd) {
        jSessionId = jSessionIdd;
    }

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String user) {
        username = user;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String pass) {
        password = pass;
    }

    public static User getCurrentUser() {
        return currentUser;
    }

    public static void setCurrentUser(User currentUser) {
        UserManager.currentUser = currentUser;
    }
}

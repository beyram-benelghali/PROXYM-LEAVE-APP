package com.proxym.proxymleave.models.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by beyram on 06/07/17.
 */

/**
 * Server Response to Create transaction
 */
public class TransactionRequestResponse {
    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("username")
    @Expose
    private String username;

    public TransactionRequestResponse() {
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

package com.proxym.proxymleave.models.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.proxym.proxymleave.models.entity.User;

/**
 * Created by beyram on 30/06/17.
 */

/**
 * Server Response to authenticate user
 */
public class LoginResponse {

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("error")
    @Expose
    private String error;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("employee")
    @Expose
    private User employee;

    public LoginResponse() {
    }

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public User getEmployee() {
        return employee;
    }

    public void setEmployee(User employee) {
        this.employee = employee;
    }
}

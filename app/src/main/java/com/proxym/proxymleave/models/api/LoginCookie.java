package com.proxym.proxymleave.models.api;

import com.proxym.proxymleave.models.manager.UserManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;

/**
 * Created by beyram on 03/07/17.
 */

/**
 * CookieManager to retrieve JSESSIONID Cookies from Response after login
 */
public class LoginCookie implements CookieJar {

    private final Set<Cookie> cookieStore = new HashSet<>();

    @Override
    public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
        /**
         *Saves cookies from HTTP response
         */
        cookieStore.addAll(cookies);

        /**
         *  SAVE JSESSION COOKIES to USER MANAGER
         *  */
        for (Cookie cookie : cookieStore) {
            if (cookie.name().equals("JSESSIONID")) {
                UserManager.setJessionId(cookie.value());
            }
        }
    }

    /**
     * @param url
     * @return list Cookies Loaded
     */
    @Override
    public List<Cookie> loadForRequest(HttpUrl url) {
        /**
         * Load cookies from the jar for an HTTP request.
         * This method returns cookies that have not yet expired
         */
        List<Cookie> validCookies = new ArrayList<>();
        for (Cookie cookie : cookieStore) {
            // LogCookie(cookie);
            if (cookie.expiresAt() < System.currentTimeMillis()) {
                // invalid cookies
            } else {
                validCookies.add(cookie);
            }
        }
        return validCookies;
    }

    //Print the values of cookies - Useful for testing
    private void LogCookie(Cookie cookie) {
        System.out.println("Cookie");
        System.out.println("String: " + cookie.toString());
        System.out.println("Expires: " + cookie.expiresAt());
        System.out.println("Hash: " + cookie.hashCode());
        System.out.println("Path: " + cookie.path());
        System.out.println("Domain: " + cookie.domain());
        System.out.println("Name: " + cookie.name());
        System.out.println("Value: " + cookie.value());
        System.out.println("End Cookie");
    }

}
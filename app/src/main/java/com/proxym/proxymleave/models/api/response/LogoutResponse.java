package com.proxym.proxymleave.models.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by beyram on 03/07/17.
 */

/**
 * Server Response to logout from the server
 */
public class LogoutResponse {

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("errors")
    @Expose
    private int errors;

    @SerializedName("body")
    @Expose
    private String body;

    public LogoutResponse() {
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getErrors() {
        return errors;
    }

    public void setErrors(int errors) {
        this.errors = errors;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}

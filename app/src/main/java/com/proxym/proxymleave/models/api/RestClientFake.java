package com.proxym.proxymleave.models.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by beyram on 05/07/17.
 */

/**
 * Client for retrofit - Singleton
 */
public class RestClientFake {

    private static RestClientFake instance = null;
    Retrofit retrofit;
    private IProxymFake apiService;

    private RestClientFake() {
        Gson gson = new GsonBuilder()
                .setDateFormat("dd-MM-yyyy")
                .setLenient()
                .create();
        OkHttpClient client = new OkHttpClient.Builder()
                .cookieJar(new LoginCookie())
                .build();
        retrofit = new Retrofit.Builder()
                .baseUrl(IProxymFake.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        apiService = retrofit.create(IProxymFake.class);
    }

    public static RestClientFake getInstance() {
        if (instance == null) {
            instance = new RestClientFake();
        }
        return instance;
    }

    public IProxymFake getProxymLeaveAPI() {
        return apiService;
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}

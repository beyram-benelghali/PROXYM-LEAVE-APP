package com.proxym.proxymleave.models.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.proxym.proxymleave.models.entity.Holiday;

import java.util.List;

/**
 * Created by beyram on 06/07/17.
 */

/**
 * Server Response to load holidays
 */
public class HolidayResponse {

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("body")
    @Expose
    private List<Holiday> body;

    public HolidayResponse() {
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<Holiday> getBody() {
        return body;
    }

    public void setBody(List<Holiday> body) {
        this.body = body;
    }
}

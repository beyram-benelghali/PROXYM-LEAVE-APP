package com.proxym.proxymleave.models.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.proxym.proxymleave.models.entity.Category;

import java.util.List;

/**
 * Created by beyram on 06/07/17.
 */

/**
 * Server Response to load categories
 */
public class CategoryResponse {

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("body")
    @Expose
    private List<Category> body;

    public CategoryResponse() {
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<Category> getBody() {
        return body;
    }

    public void setBody(List<Category> body) {
        this.body = body;
    }
}

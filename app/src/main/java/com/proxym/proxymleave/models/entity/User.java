package com.proxym.proxymleave.models.entity;

import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by beyram on 28/06/17.
 */

public class User {
    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("displayName")
    @Expose
    private String displayName;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("department")
    @Expose
    private JsonObject department;

    public User() {
    }

    public JsonObject getDepartment() {
        return department;
    }

    public void setDepartment(JsonObject department) {
        this.department = department;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

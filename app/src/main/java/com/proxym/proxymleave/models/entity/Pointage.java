package com.proxym.proxymleave.models.entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by beyram on 7/27/17.
 */

public class Pointage {

    @SerializedName("id")
    @Expose
    private int id;

    @SerializedName("time")
    @Expose
    private String time;

    @SerializedName("employeeId")
    @Expose
    private int employeeId;

    @SerializedName("employeeUsername")
    @Expose
    private String employeeUsername;

    @SerializedName("employeeMatricule")
    @Expose
    private String employeeMatricule;

    @SerializedName("pourcentage")
    @Expose
    private int pourcentage;

    @SerializedName("pointageTimes")
    @Expose
    private ArrayList<Date> pointageTimes;

    public Pointage() {
    }

    public ArrayList<Date> getPointageTimes() {
        return pointageTimes;
    }

    public void setPointageTimes(ArrayList<Date> pointageTimes) {
        this.pointageTimes = pointageTimes;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeUsername() {
        return employeeUsername;
    }

    public void setEmployeeUsername(String employeeUsername) {
        this.employeeUsername = employeeUsername;
    }

    public String getEmployeeMatricule() {
        return employeeMatricule;
    }

    public void setEmployeeMatricule(String employeeMatricule) {
        this.employeeMatricule = employeeMatricule;
    }

    public int getPourcentage() {
        return pourcentage;
    }

    public void setPourcentage(int pourcentage) {
        this.pourcentage = pourcentage;
    }
}
